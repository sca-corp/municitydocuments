//========================================================
// Municity Node
// Notification Services
//
// Prefix:              /notification
// Currently Supports:  /
//
// Author:          Caleb Wright <cwright@sca-corp.com>
// Created Date:    5/9/2014
// Last Modified:   11/20/2018
//========================================================

var express     = require('express');
var nodemailer  = require('nodemailer');
var router      = express.Router();
let transportMunicity5 = {};

try { transportMunicity5 = JSON.parse(process.env['TransportMunicity5']); } catch(e) {}

let transportAWS = {};
try { transportAWS = JSON.parse(process.env['TransportAWS']); } catch(e) {}


var Municity5Transport = nodemailer.createTransport({
    host: transportAWS.host, // hostname
    secureConnection: true, // use SSL
    port: transportAWS.port, // port for secure SMTP
    auth: {
        user: transportAWS.user,
        pass: transportAWS.pass,
    }
});

router.post('/', function(req, res) {

    //req.logStream.write(JSON.stringify(req.body)+'\r\n');
    var message      = req.body.Description;
    var emailAddress = req.body.Email;
    var phoneNumber  = req.body.Phone;
    var user         = req.body.UserName;
    var carrier      = req.body.Carrier;
    var sendEmail    = req.body.SendEmail;
    var sendText     = req.body.SendText;

    var carrierEmail = {
        Verizon : "vtext.com",
        Sprint  : "messaging.sprintpcs.com",
        TMobile : "tmomail.net",
        ATT     : "txt.att.net"
    };

    // COMPOSE EMAIL
    var notificationEmail = {
        from    : "municity5support@municitysoftware.com",
        to      : emailAddress,
        subject : "Municity 5 Notification",
        html    : user + ": " + message,
        forceEmbeddedImages :true
    };

    // ADD OR REPLACE WITH TEXT
    if (sendEmail === 1 && sendText === 1) {
        notificationEmail.to += ';' + phoneNumber + '@' + carrierEmail[carrier];
    } else if (sendEmail !== 1 && sendText === 1) {
        notificationEmail.to = phoneNumber + '@' + carrierEmail[carrier];    	
    }

    // SEND FROM MUNICITY 5 AMAZON SES
    if(sendEmail === 1 || sendText === 1){
        Municity5Transport.sendMail(notificationEmail, function(error, response){
            if(error){
                res.send({Success:false, Response:'Failed to Send Email'});
            }else{
                res.send({Success:true, Response:'Message Sent Successfully'});
            }
        });
    } else if(sendEmail !== 1 && sendText !== 1){
        res.send({Success:true, Response:'Nothing Sent'});
    }
});

module.exports = router;