//========================================================
// Municity Node
// Exports
//
// Prefix:              /export
// Currently Supports:  /csv, /txt
//
// Author:          Caleb Wright <cwright@sca-corp.com>
// Created Date:    5/12/2014
// Last Modified:   5/12/2014
//========================================================

var express = require('express');
var router 	= express.Router();
var fs		= require('fs');
var nodefs	= require('node-fs');

var mediaDirectory = 'D:/Municity Media/';

router.post('/csv', function(req, res) {

	//req.logStream.write(JSON.stringify(req.body)+'\r\n');
  	var fileName 	= req.body.fileName;
  	var filePath	= req.body.filePath;
  	var csvString 	= req.body.csvString;

  	nodefs.mkdir(mediaDirectory + filePath, 0777, true, function(err){
		if(err){
			//req.logStream.write('Error Creating Directory: ' + mediaDirectory + filePath + '\r\n' + err + '\r\n');
			res.send({Success:false, Response:'Failed to Create Directory'});
		}else{
			fs.writeFile(mediaDirectory + filePath + '/' + fileName + '.csv', csvString, function (err) {
		  		res.send({Success:true, Response:'CSV Created Successfully', Reference:'https://municitymedia.com/' + filePath + '/' + fileName + '.csv'});
			});
		}
	});
  	
});

router.post('/txt', function(req, res) {

	//req.logStream.write(JSON.stringify(req.body)+'\r\n');
  	var fileName 	= req.body.fileName;
  	var filePath	= req.body.filePath;
  	var dataString 	= req.body.dataString;

  	nodefs.mkdir(mediaDirectory + filePath, 0777, true, function(err){
		if(err){
			//req.logStream.write('Error Creating Directory: ' + mediaDirectory + filePath + '\r\n' + err + '\r\n');
			res.send({Success:false, Response:'Failed to Create Directory'});
		}else{
			fs.writeFile(mediaDirectory + filePath + '/' + fileName + '.txt', dataString, function (err) {
		  		res.send({Success:true, Response:'TXT Created Successfully', Reference:'https://municitymedia.com/' + filePath + '/' + fileName + '.txt'});
			});
		}
	});
  	
});

module.exports = router;
