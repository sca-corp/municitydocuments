//Making web requests
var request = require('request');

//To help with pluralizing entity names
var pluralize = require('pluralize');

class DataExtractor {

  //Constructor
  constructor(opts){
    this._customer    = opts.customer;
    this._debugMode   = opts.debugMode;
    this._servicesUrl = opts.servicesUrl;
    this._token       = opts.token;
    this._username    = opts.username;
    this._contactId   = opts.contactId;
  }

  //Getters
  get headers(){
    return {
      Authorization: 'Token ' + this._token
    };
  }
  get contactId(){
    return this._contactId;
  }
  get customer(){
    return this._customer;
  }
  get debugMode(){
    return this._debugMode;
  }
  get servicesUrl(){
    return this._servicesUrl;
  }
  get specialEntities(){
    return {
      'ParentOwner': {
        Type: 'Contact',
        IDProperty: 'Contact_ID'
      },
      'Type': {},
      'Status': {}
    };
  }
  get token(){
    return this._token;
  }
  get username(){
    return this._username;
  }

  getDocumentData(entity, entityId, documentTags){
    var me = this;

    var entityTree = me.buildEntityTree(entity, entityId, documentTags);

    return new Promise(function(resolve, reject){
      Promise.all([
        me.getMunicipalityInfo(),
        me.getContactRoles(),
        me.getColumnInfo(),
        me.getUserDefinedFields(),
        me.getBreadcrumbs(entity, entityId)
      ])
      .then(function(results){
        var municipalityInfo = results[0];
        var contactRoles = results[1];
        var columnInfo = results[2];
        var userDefinedFields = results[3];
        var breadcrumbs = results[4];

        //Try to fill in PrintedBy
        if(entityTree.Relationships.PrintedBy){
          if(me.contactId){
            entityTree.Relationships.PrintedBy.Meta.ID = me.contactId;
            entityTree.Relationships.PrintedBy.Meta.IDProperty = 'Contact_ID';
            entityTree.Relationships.PrintedBy.Meta.Type = 'Contact';
          }
          else{
            entityTree.Relationships.PrintedBy.Meta.Loaded = true;
          }
        }

        //Fill in breadcrumb data
        for(let relationshipEntity in entityTree.Relationships){
          let relationship = entityTree.Relationships[relationshipEntity];
          for(var i = 0; i < breadcrumbs.length; i++){
            let breadcrumb = breadcrumbs[i];
            if(breadcrumb.Entity === relationshipEntity){
              relationship.Meta.ID = breadcrumb.Id;
            }
          }

          if(relationshipEntity === 'Parent'){
            var foundParent = null;
            for(var i = 0; i < breadcrumbs.length; i++){
              if(breadcrumbs[i].Entity !== 'Project' && !foundParent){
                foundParent = breadcrumbs[i];
                break;
              }
            }

            if(foundParent){
              relationship.Meta.ID = foundParent.Id;
              relationship.Meta.Type = foundParent.Entity;
              relationship.Meta.IDProperty = foundParent.Entity + '_ID';
            }
            else{
              delete entityTree.Relationships[relationshipEntity];
            }
          }
        }

        //Convert contact roles to Contact entities and update Meta
        me.propegateContactRoles(entityTree, contactRoles);

        //Fill in fields
        me.propegateColumnInfoRecords(entityTree, columnInfo);
        me.propegateUserDefinedRecords(entityTree, userDefinedFields);

        me.populateData(entityTree, function(loadedEntityTree){

          var printoutData = me.printoutifyData(loadedEntityTree);

          //Toss in the Municipality Information
          printoutData.Municipality = municipalityInfo;

          resolve(printoutData);
        });
      })
      .catch(function(error){
        if(me.debugMode){
          reject(error);
        }
        else{
          reject('An error occurred.');
        }
      });
    });
  }

  /**
    * Base function to start building an entityTree
    * @param {string} entity The entity type
    * @param {int} entityId The ID of the entity
    * @param {object} documentTags The document tags from the document
    *
    * @return {object} The entityTree scaffolding
  **/
  buildEntityTree(entity, entityId, documentTags){
    let me = this;

    var entityTree = me.createEntityObject(entity);
    entityTree.Meta.ID = entityId;

    return me.buildEntityTreeHelper(entityTree, documentTags);
  }

  /**
    * Helper function to build the entityTree, calls itself as it navigates
    * through the structure.
    * @param {object} baseEntityTree The base object to start on
    * @param {object} documentTags The document tags from the document
    *
    * @return {object} Returns a completed entity tree
    *
    * private
    * @param {array} path Pointer to where we currently are in the entityTree
  **/
  buildEntityTreeHelper(baseEntityTree, documentTags, path){
    var me = this;
    path = path || [];

    for(var tagName in documentTags){
      //Field
      if(me.isObjectEmpty(documentTags[tagName])){
        let splitTag = tagName.split('.');
        var newPath = path.slice();
        for(var i = 0; i < splitTag.length - 1; i++){
          newPath.push('Relationships');
          var newEntity = splitTag[i];
          var newPointer = me.keyPathAccessor(baseEntityTree, newPath);
          if(!newPointer[newEntity]){
            newPointer[newEntity] = me.createEntityObject(newEntity);
          }
          newPath.push(newEntity);
        }
        me.keyPathAccessor(baseEntityTree, newPath).Meta.RawFields.push(splitTag.pop().split('|').shift());
      }
      //Relationship
      else{
        let splitTag = tagName.split('.');
        var newPath = path.slice();
        for(var i = 0; i < splitTag.length; i++){
          newPath.push('Relationships');
          var newEntity = splitTag[i];
          var newPointer = me.keyPathAccessor(baseEntityTree, newPath);
          if(!newPointer[newEntity]){
            newPointer[newEntity] = me.createEntityObject(newEntity);
          }
          newPath.push(newEntity);
        }
        me.buildEntityTreeHelper(baseEntityTree, documentTags[tagName], newPath);
      }
    }
    return baseEntityTree;
  }

  /**
    * Function to create a template for an entity object
    * @param {string} entityName The entity name
    *
    * @return {object} Returns an entity object template
  **/
  createEntityObject(entityName){

    let entityObject = {
      Relationships: {},
      Data: {},
      Meta: {
        Fields: [],
        Loaded: false,
        RawFields: [],
        Type: pluralize.singular(entityName),
        ID: null,
        IDProperty: pluralize.singular(entityName) + '_ID',
        RoleGroupCodeID: null
      }
    };

    let specialEntities = this.specialEntities;
    let specialEntity = specialEntities[pluralize.singular(entityName)];

    if(specialEntity){
      if(specialEntity.Type){
        entityObject.Meta.Type = specialEntity.Type;
      }
      if(specialEntity.IDProperty){
        entityObject.Meta.IDProperty = specialEntity.IDProperty;
      }
    }

    return entityObject;
  }

  checkToken(){
    let headers = this.headers;
    let token = this.token;

    let url = this.servicesUrl;
    url += '/Customer';
    url += '/' + this.username;
    url += '/' + token;
    url += '/Check';

    return new Promise(function(resolve, reject){
      request(
        {
          method: 'GET',
          url: url,
          headers: headers,
          gzip: true,
          json: true
        },
        function (error, response, data) {
      		if (!error && response.statusCode == 200) {
      			resolve(true);
      		}
          else {
            reject({
              url: url,
              headers: headers,
              statusCode: response.statusCode,
              data: data,
              location: 'checkToken'
            });
          }
      	}
      );
    });
  }

  getColumnInfo(){
    let headers = this.headers;

    let url = this.servicesUrl;
    url += '/Customer';
    url += '/' + this.customer;
    url += '/ColumnInfo';

    return new Promise(function(resolve, reject){
      request(
        {
          method: 'GET',
          url: url,
          headers: headers,
          gzip: true,
          json: true
        },
        function (error, response, data) {
      		if (!error && response.statusCode == 200) {
        			resolve(data);
      		}
          else {
            reject({
              url: url,
              headers: headers,
              statusCode: response.statusCode,
              data: data,
              location: 'getColumnInfo'
            });
          }
      	}
      );
    });
  }

  getUserDefinedFields(){
    let headers = this.headers;

    let url = this.servicesUrl;
    url += '/Customer';
    url += '/' + this.customer;
    url += '/UserDefinedFields';

    return new Promise(function(resolve, reject){
      request(
        {
          method: 'GET',
          url: url,
          headers: headers,
          gzip: true,
          json: true
        },
        function (error, response, data) {
      		if (!error && response.statusCode == 200) {
      			resolve(data);
      		}
          else {
            reject({
              url: url,
              headers: headers,
              statusCode: response.statusCode,
              data: data,
              location: 'getUserDefinedFields'
            });
          }
      	}
      );
    });
  }

  getContactRoles(){
    let headers = this.headers;

    let url = this.servicesUrl;
    url += '/Customer';
    url += '/' + this.customer;
    url += '/RoleGroup';

    return new Promise(function(resolve, reject){
      request(
        {
          method: 'GET',
          url: url,
          headers: headers,
          gzip: true,
          json: true
        },
        function (error, response, data) {
          if (!error && response.statusCode == 200) {
            resolve(data);
          }
          else {
            reject({
              url: url,
              headers: headers,
              statusCode: response.statusCode,
              data: data,
              location: 'getContactRoles'
            });
          }
        }
      );
    });
  }

  getMunicipalityInfo(){
    let headers = this.headers;

    let url = this.servicesUrl;
    url += '/Customer';
    url += '/' + this.customer;
    url += '/CustomerInfo';

    return new Promise(function(resolve, reject){
      request(
        {
          method: 'GET',
          url: url,
          headers: headers,
          gzip: true,
          json: true
        },
        function (error, response, data) {
          if (!error && response.statusCode == 200) {
            resolve(data);
          }
          else {
            reject({
              url: url,
              headers: headers,
              statusCode: response.statusCode,
              data: data,
              location: 'getMunicipalityInfo'
            });
          }
        }
      );
    });
  }

  /**
    * Check for a Meta.Type in the entity tree.
    * @param {object} entityTree The entity tree to search through.
    * @param {string} entity The Meta.Type to look for.
    * @return {array} An array of objects with the Parent, Child (Meta.Type) and
    *                 the RelationshipName.
  **/
  checkForEntity(entityTree, entity){
    let entities = [];

    for(let relationshipName in entityTree.Relationships){
      let relationship = entityTree.Relationships[relationshipName];

      if(relationship.Meta.Type === entity){
        entities.push({
          Parent: entityTree.Meta.Type,
          Child: relationship.Meta.Type,
          RelationshipName: relationshipName
        });
      }

      entities = entities.concat(this.checkForEntity(relationship, entity));
    }

    return entities;
  }

  /**
    * Get breadcrumbs for an entity, helpful for finding parents.
    * @param {string} entity The type of the entity
    * @param {int} entityId The ID of the entity
  **/
  getBreadcrumbs(entity, entityId){
    let headers = this.headers;

    let url = this.servicesUrl;
    url += '/M5BreadCrumbs';
    url += '?customer=' + this.customer;
    url += '&Entity=' + entity;
    url += '&EntityID=' + entityId;

    return new Promise(function(resolve, reject){
      request(
        {
          method: 'GET',
          url: url,
          headers: headers,
          gzip: true,
          json: true
        },
        function (error, response, data) {
          if (!error && response.statusCode == 200) {
            let breadcrumbs = data.rows;
            resolve(breadcrumbs);
          }
          else {
            reject({
              url: url,
              headers: headers,
              entity: entity,
              entityId: entityId,
              statusCode: response.statusCode,
              data: data,
              location: 'getBreadcrumbs'
            });
          }
        }
      );
    });
  }

  //TODO remove parent?
  /**
    * Get the data requested by the printout.
    * Pretty big thing
    * @param {object} entityTree The entityTree defining which things to get
    * @param {function} callback Callback function, retuns a loaded entityTree
    *
    * private
    * @param {object} parent The parent object
  **/
  populateData (entityTree, callback, parent){
    var me = this;
    if(!parent){

      let url = this.servicesUrl;
      url += '/Customer/' + this.customer;
      url += '/FilteredSearch';

      let baseEntity = {
        Base: entityTree.Meta.Type,
        Fields: [],
        Filters: [{
          Entity: entityTree.Meta.Type,
          Name: entityTree.Meta.Type + '_ID',
          Operator: 'Equals',
          Value: entityTree.Meta.ID
        }]
      };

      for(let i = 0; i < entityTree.Meta.Fields.length; i++){
        baseEntity.Fields.push({
          [entityTree.Meta.Type]: entityTree.Meta.Fields[i].Name
        });
      }

      request(
        {
          method: 'POST',
          url: url,
          headers: this.headers,
          gzip: true,
          body: baseEntity,
          json: true
        },
        function (error, response, data) {
          if (!error && response.statusCode == 200) {

            let res = data.Data[0];
            entityTree.Data = res;
            entityTree.Meta.Loaded = true;
            if(!me.isObjectEmpty(entityTree.Relationships)){

              me.populateData(
                entityTree,
                function(childEntityTree){
                  callback(childEntityTree);
                },
                entityTree
              );
            }
            else{
              callback(entityTree);
            }
          }
          else {
            callback(entityTree);
          }
        }
      );
    }
    else{
      if(entityTree.Meta.Loaded){
        let url = this.servicesUrl;
        url += '/Customer/' + this.customer;
        url += '/FilteredSearch';

        let queries = [];
        let unloadedEntityIndex = [];

        for(var relationshipName in entityTree.Relationships){
          let relationship = entityTree.Relationships[relationshipName];
          if(!relationship.Meta.Loaded){

            let fields = [];
            for(let i = 0; i < relationship.Meta.Fields.length; i++){
              fields.push({
                [relationship.Meta.Type]: relationship.Meta.Fields[i].Name
              });
            }

            if(!relationship.Meta.ID){
              fields.push({[relationship.Meta.Type]: relationship.Meta.IDProperty});
            }

            let specialEntity = me.specialEntities[relationshipName];
            if(!specialEntity){
              if(relationship.Meta.ID){
                if(fields.length > 0){
                  queries.push({
                    Base: relationship.Meta.Type,
                    Fields: fields,
                    Filters: [{
                      Entity: relationship.Meta.Type,
                      Name: relationship.Meta.Type + '_ID',
                      Operator: 'Equals',
                      Value: relationship.Meta.ID
                    }]
                  });

                  unloadedEntityIndex.push(relationshipName)
                }
                else{
                  relationship.Meta.Loaded = true;
                }
              }
              else{
                var queryFilter = {
                    Entity: entityTree.Meta.Type,
                    Name: entityTree.Meta.Type + '_ID',
                    Operator: 'Equals',
                    Value: entityTree.Meta.ID
                };

                //If the current entity is a list of items
                if(entityTree.Meta.ID.constructor === Array){
                  queryFilter.Operator = 'in';
                  fields.push({[entityTree.Meta.Type]: entityTree.Meta.IDProperty});
                }

                var queryRelationship = {
                  Base: relationship.Meta.Type
                };

                if(relationship.Meta.RoleGroupCodeID){
                  queryRelationship.RoleGroupCode_ID = relationship.Meta.RoleGroupCodeID;
                }

                queries.push({
                  Base: entityTree.Meta.Type,
                  Fields: fields,
                  Filters: [queryFilter],
                  Relationships: {
                    Children: [queryRelationship]
                  }
                });

                unloadedEntityIndex.push(relationshipName)
              }

            }
          }
        }

        if(queries.length > 0){
          request(
            {
              method: 'POST',
              url: url,
              headers: this.headers,
              gzip: true,
              body: queries,
              json: true
            },
            function (error, response, data) {
              if (!error && response.statusCode == 200) {
                for(var i = 0; i < unloadedEntityIndex.length; i++){

                  var relationshipResponse = data[i];

                  let relationshipName = unloadedEntityIndex[i];
                  let relationship = entityTree.Relationships[relationshipName];
                  let fields = relationship.Meta.Fields;

                  relationship.Meta.Loaded = true;
                  if(relationshipResponse.Count > 0){
                    relationship.Data = relationshipResponse.Data;

                    if(relationshipResponse.Count > 1){
                      let ids = [];

                      for(var j = 0; j < relationshipResponse.Count; j++){
                        ids.push(relationshipResponse.Data[j][relationship.Meta.IDProperty]);
                      }

                      relationship.Meta.ID = ids;

                      //Make sure the fieldnames match the RawName
                      if(fields.length > 0){
                        for(var j = 0; j < relationship.Data.length; j++){
                          let record = relationship.Data[i];
                          for(var fieldName in record){
                            for(let k = 0; k < fields.length; k++){
                              let field = fields[k];
                              if(field.FieldName === fieldName){
                                record[field.RawName] = record[fieldName];
                              }
                            }
                          }
                        }
                      }
                    }
                    else{
                      relationship.Data = relationshipResponse.Data[0];

                      if(!relationship.Data[entityTree.Meta.IDProperty]){
                        relationship.Data[entityTree.Meta.IDProperty] = entityTree.Meta.ID;
                      }

                      if(!relationship.Meta.ID){
                        relationship.Meta.ID = relationshipResponse.Data[0][relationship.Meta.IDProperty];
                      }

                      //Make sure the fieldnames match the RawName
                      if(fields.length > 0){
                        let record = relationship.Data;
                        for(var fieldName in record){
                          for(let k = 0; k < fields.length; k++){
                            let field = fields[k];
                            if(field.FieldName === fieldName){
                              record[field.RawName] = record[fieldName];
                            }
                          }
                        }
                      }
                    }
                  }
                }

                //If it has children, load them
                if(!me.isObjectEmpty(entityTree.Relationships)){

                  var loadedCount = 0;
                  var totalCount = 0;

                  for(let relationshipName in entityTree.Relationships){

                    totalCount++;

                    let relationship = entityTree.Relationships[relationshipName];
                    me.populateData(
                      relationship,
                      function(childEntityTree){
                        relationship = childEntityTree;
                        loadedCount++;
                        if(loadedCount === totalCount){
                          callback(entityTree);
                        }
                      },
                      entityTree
                    );
                  }
                }
                else{
                  console.log('entity tree relationships empty');
                  callback(entityTree);
                }
              }
              else {
                console.log(error);
                callback(entityTree);
              }
            }
          );
        }
        else{
          callback(entityTree);
        }
      }
      else{
        callback(entityTree);
      }
    }
  }

  /**
    * Flatten the entityTree so it will fit the printout tags
    * @param {object} entityTree The entity tree to convert into printout format
    * @return {object} Returns formatted printoutData
    *
    * private
    * @param {string} parentIdProperty The parent ID property name when matching
    *                                  many to many relationships
    * @param {int} parentId The parent ID when matching mant to many
    *                       relationships
  **/
  printoutifyData (entityTree, parentIdProperty, parentId){

    var dataIsArray = entityTree.Data.constructor === Array;
    var printoutData = dataIsArray ? [] : {};

    //Load data
    for(let i in entityTree.Data){

      if(dataIsArray){
        if(parentIdProperty && parentId){
          if(entityTree.Data[i][parentIdProperty] === parentId){
            printoutData.push(entityTree.Data[i]);
          }
        }
        else{
          printoutData.push(entityTree.Data[i]);
        }
      }
      else{
        if(parentIdProperty && parentId){
          if(entityTree.Data[parentIdProperty] === parentId){
            printoutData[i] = entityTree.Data[i];
          }
        }
        else{
          printoutData[i] = entityTree.Data[i];
        }
      }
    }

    for(let relationshipName in entityTree.Relationships){

      var idProperty = entityTree.Meta.IDProperty;

      if(dataIsArray){
        for(let i = 0; i < printoutData.length; i++){

          if(!printoutData[i][relationshipName]){
            printoutData[i][relationshipName] = [];
          }

          let id = printoutData[i][idProperty];
          let child = this.printoutifyData(entityTree.Relationships[relationshipName], idProperty, id);
          if(child){
            printoutData[i][relationshipName].push(child);
          }
        }
      }
      else{
        let child = this.printoutifyData(entityTree.Relationships[relationshipName], idProperty, entityTree.Meta.id);
        if(child){
          printoutData[relationshipName] = child;
        }
      }
    }

    return this.isObjectEmpty(printoutData) ? null : printoutData;
  }

  /**
    * Fill in the fields attribute of an entityTree.
    * Matches up on FriendlyName or ColumnName.
    * @param {object} entityTree The entityTree to update
    * @param {array} columnInfoRecords An array of ColumnInfo records to map
    *
    * private
    * @param {array} path Pointer to where we are in the entityTree
  **/
  propegateColumnInfoRecords (entityTree, columnInfoRecords, path){
    path = path || [];

    let pathPointer = this.keyPathAccessor(entityTree, path);
    let entity = pathPointer.Meta.Type;
    let specialEntities = this.specialEntities;
    if(specialEntities[entity]){
      entity = specialEntities[entity].Type;
    }

    for(let i = 0; i < pathPointer.Meta.RawFields.length; i++){
      let rawField = pathPointer.Meta.RawFields[i];
      for(let j = 0; j < columnInfoRecords.length; j++){
        let columnInfoRecord = columnInfoRecords[j];
        if(columnInfoRecord.Entity === entity){
          let friendlyName = columnInfoRecord.FriendlyName.replace(/ /g, '');
          if(friendlyName === rawField || columnInfoRecord.ColumnName === rawField){
            pathPointer.Meta.Fields.push({
              FriendlyName: friendlyName,
              Name: columnInfoRecord.ColumnName,
              RawName: rawField,
              Type: columnInfoRecord.FieldType
            });
            break;
          }
        }
      }
    }

    for(let relationship in pathPointer.Relationships){
      let newPath = path.slice();
      newPath.push('Relationships', relationship);
      this.propegateColumnInfoRecords(entityTree, columnInfoRecords, newPath);
    }
  }

  /**
    * See if any of the types match up with Contact Roles. If they do, change
    * Meta.Type to Contact and fill in Meta.RoleGroupCodeID.
    * @param {object} entityTree The entityTree to update
    * @param {array} contactRoles An array of COntactRoles to go through
    *
    * private
    * @param {array} path Pointer to where we are in the entityTree
  **/
  propegateContactRoles (entityTree, contactRoles, path){
    path = path || [];

    let pathPointer = this.keyPathAccessor(entityTree, path);
    let entity = pathPointer.Meta.Type.toLowerCase();
    let singularEntity = pluralize.singular(entity);

    let matchedContactRole = null;

    //Prioritize Code column
    for(let i = 0; i < contactRoles.length; i++){
      let contactRole = contactRoles[i];
      let contactCode = contactRole.Code || '';
      contactCode = contactCode.replace(/ /g, '');
      contactCode = contactCode.toLowerCase();

      if(contactCode === entity || contactCode === singularEntity){
        matchedContactRole = contactRole;
      }
    }

    //If we didn't match up on the Code column, try to check for a flag
    if(!matchedContactRole){
      for(let i = 0; i < contactRoles.length; i++){
        let contactRole = contactRoles[i];

        let contactRoleKeys = {};
        for(let key in contactRole) {
          contactRoleKeys[key.toLowerCase()] = contactRole[key];
        }

        if(contactRoleKeys[entity] || contactRoleKeys[singularEntity]){
          matchedContactRole = contactRole;
        }
      }
    }

    if(matchedContactRole){
      pathPointer.Meta.Type = 'Contact';
      pathPointer.Meta.IDProperty = 'Contact_ID';
      pathPointer.Meta.RoleGroupCodeID = matchedContactRole.Code_ID;
    }

    for(let relationship in pathPointer.Relationships){
      let newPath = path.slice();
      newPath.push('Relationships', relationship);
      this.propegateContactRoles(entityTree, contactRoles, newPath);
    }
  }

  /**
    * Fill in the fields attribute of an entityTree.
    * Matches up on FieldName.
    * @param {object} entityTree The entityTree to update
    * @param {array} columnInfoRecords An array of UserDefinedField records to
    *                                  map
    *
    * private
    * @param {array} path Pointer to where we are in the entityTree
  **/
  propegateUserDefinedRecords (entityTree, userDefinedRecords, path){
    path = path || [];

    let pathPointer = this.keyPathAccessor(entityTree, path);
    let entity = pathPointer.Meta.Type;
    let specialEntities = this.specialEntities;
    if(specialEntities[entity]){
      entity = specialEntities[entity].Type;
    }

    for(let i = 0; i < pathPointer.Meta.RawFields.length; i++){
      let rawField = pathPointer.Meta.RawFields[i];
      for(let j = 0; j < userDefinedRecords.length; j++){
        let userDefinedRecord = userDefinedRecords[j];
        if(userDefinedRecord.Entity === entity){
          let fieldName = userDefinedRecord.FieldName.replace(/ /g, '');
          if(fieldName === rawField){
            pathPointer.Meta.Fields.push({
              FriendlyName: fieldName,
              Name: userDefinedRecord.FieldName,
              RawName: rawField,
              Type: userDefinedRecord.Selector
            });
            break;
          }
        }
      }
    }

    for(let relationship in pathPointer.Relationships){
      let newPath = path.slice();
      newPath.push('Relationships', relationship);
      this.propegateUserDefinedRecords(entityTree, userDefinedRecords, newPath);
    }
  }

  /**
    * Check whether or not an object is empty.
    * @param {object} obj The object to check
    *
    * @return {boolean} Returns whether or not the object was empty
  **/
  isObjectEmpty(obj){
    for(let prop in obj){
      if(obj.hasOwnProperty(prop)){
        return false;
      }
    }
    return JSON.stringify(obj) === JSON.stringify({});
  }

  /**
    * Access a spot in an object, can be multiple levels deep.
    * @param {object} obj The object you are trying to access
    * @param {array} path Where you want to look in the object
    *
    * @return {value} Returns the requested spot in the object
  **/
  keyPathAccessor(obj, path){
    let res = obj;
    for (let i = 0; i < path.length; i++){
      res = res[path[i]];
    }
    return res;
  }
}

module.exports = DataExtractor;
