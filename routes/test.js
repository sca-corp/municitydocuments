//========================================================
// Municity Node
// Email Services
//
// Prefix:              /email
// Currently Supports:  /m5password, /custom
//
// Author:          Caleb Wright <cwright@sca-corp.com>
// Created Date:    5/9/2014
// Last Modified:   10/30/2015
//========================================================

var express 	= require('express');
var nodemailer 	= require('nodemailer');
var router 		= express.Router();

var GmailTransport = nodemailer.createTransport(,{
    service: "Gmail",
    secureConnection: true,
    auth: {
        user: "Municity5@gmail.com",
        pass: "71gis45municity"
    }
});

var Municity5Transport = nodemailer.createTransport(, {
	host: "email-smtp.us-east-1.amazonaws.com", // hostname
	secureConnection: true, // use SSL
	port: 465, // port for secure SMTP
	auth: {
	    user: "AKIAIH3JK566VHRV2KBA",
	    pass: "AurFYpeFsSyhKuvtmE4vnjXRA5/gy6iAMCR9MGx3dHjM"
	}
});

router.post('/m5password', function(req, res) {

	//req.logStream.write(JSON.stringify(req.body)+'\r\n');
  	var recipientEmail 	= req.body.recipientEmail;
  	var recipientName 	= req.body.recipientName;
  	var username 		    = req.body.username;
  	var password 		    = req.body.password;

  	//COMPOSE EMAIL
	var email = {
    	from: "The Municity5 Team <Municity5@gmail.com>", // sender address
      	to: recipient, // list of receivers
      	subject: 'Municity5 Account Registration', // Subject line
      	html: 	'<p><img src="https://municitymedia.com/M5EmailTemplate/Header.png"/></p>'+
        		'<p>'+recipientName+',<br><br>You have requested that your password be emailed to you.<br><br>'+
        		'Sign In At http://Municity5.com/Municity5<br>Your Username: '+username+'<br>Your Password: '+password+'<br><br>-The Municity5 Team</p>'+
            	'<p><img src="https://municitymedia.com/M5EmailTemplate/Footer.png"/></p>',
      	forceEmbeddedImages:true
    };
    //SEND EMAIL FROM MUNICITY 5 GMAIL
    GmailTransport.sendMail(email, function(error, response){
        if(error){
        	//req.logStream.write('Failed to Send Email: ' + '\r\n' + error + '\r\n');
            res.send({Success:false, Response:'Failed to Send Email'});
        }else{
            res.send({Success:true, Response:'Email Sent Successfully'});
        }
    });
	
});

router.post('/custom', function(req, res) {

	//req.logStream.write(JSON.stringify(req.body)+'\r\n');
	  var sendFrom 		   = req.body.sendFrom;
  	var recipients 	   = req.body.recipients;
  	var subject 		   = req.body.subject;
  	var message 		   = req.body.message;
    var attachmentName = req.body.attachmentName;
    var attachmentPath = req.body.attachmentPath;

  	//COMPOSE EMAIL
	var customEmail = {
    	from: sendFrom, // sender address
      to: recipients, // list of receivers
      subject: subject, // Subject line
      html: message,
      forceEmbeddedImages:true,
      attachments: [
        {
          fileName: attachmentName,
          filePath: attachmentPath
        }
      ]
    };
    //SEND FROM MUNICITY 5 AMAZON SES
    Municity5Transport.sendMail(customEmail, function(error, response){
        if(error){
        	//req.logStream.write('Failed to Send Email: ' + '\r\n' + error + '\r\n');
            res.send({Success:false, Response:'Failed to Send Email'});
        }else{
        	//req.logStream.write('Email Sent: ' + '\r\n' + JSON.stringify(response) + '\r\n');
            res.send({Success:true, Response:'Email Sent Successfully'});
        }
    });
});

module.exports = router;