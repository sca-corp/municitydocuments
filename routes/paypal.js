//========================================================
// Municity Node
// PayPal Services
//
// Prefix:              /paypal
// Currently Supports:  /authorization
//
// Author:          Caleb Wright <cwright@sca-corp.com>
// Created Date:    5/9/2014
// Last Modified:   5/9/2014
//========================================================

var express = require('express');
var request = require('request');
var router 	= express.Router();

//KEEP TOKEN ALIVE, CHECK CREATED DATE AGAINST CURRENT TIME (TOKEN VALID FOR 8 HOURS)
var token = ''
var tokenCreated = '';

router.post('/authorization', function(req, res) {

	//req.logStream.write(JSON.stringify(req.body)+'\r\n');
  	var customer = req.body.customer;

  	//GET PAYMENT USERNAME AND PASSWORD FROM M5SERVICES (SERVICE ACCESS DB)
  	request.get("http://municity5.com/M5Services/M5Service.svc/GetM5Data?"+
  		"customer=Municity&module=&section=&entity=TenantPayment"+
  		"&entity_id=&mapobject=&mapid=&pid=&searchfield=TenantName"+
  		"&fieldtype=text&operator==&value="+customer+"&numrecs=100", function(error, response, body){
		if(error){
			//req.logStream.write('M5 Service Failed \r\n');
			res.send({Success:false, Response:'M5 Service Failed'});
		}else{
			//GET USERNAME AND PASSWORD FROM CALL
			var body = JSON.parse(body);
			var username = body.rows[0].PaymentUsername;
  			var password = body.rows[0].PaymentPassword;
  			//CREATE PAYPAL AUTHENTICATION STRING, ENCODE username:password TO BASE64
			var auth = new Buffer(username+':'+password).toString('base64');
			var options = {
			    url: 'https://api.sandbox.paypal.com/v1/oauth2/token',
			    method: 'POST',
			    headers: {
			        'Authorization': 'Basic ' + auth,
			        'Accept-Language': 'en_US',
			        'Content-type': 'application/x-www-form-urlencoded'
			    },
			    body: 'grant_type=client_credentials'
			};
			//REQUEST PAYPAL TOKEN
			request(options, function(error, response, body){
				if(error){
					//req.logStream.write('PayPal Auth Call Failed \r\n');
					res.send({Success:false, Response:'Authentication Failed'});
				}else{
					var body = JSON.parse(body);
					res.send({Success:true, Response:'Authentication complete', Token:body.access_token});
				}	
			});

		}	
	});
	
});

module.exports = router;