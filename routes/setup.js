//========================================================
// Municity Node
// New Customer Setup
//
// Prefix:              /setup
// Currently Supports:  /
//
// Author:          Caleb Wright <cwright@sca-corp.com>
// Created Date:    5/9/2014
// Last Modified:   5/9/2014
//========================================================

var express = require('express');
var router = express.Router();

router.get('/', function(req, res) {

	//req.logStream.write(JSON.stringify(req.body)+'\r\n');
  	var customer = req.body.customer;

	res.send({Success:true, Response:'Setup for '+customer+' complete.'});
});

module.exports = router;
