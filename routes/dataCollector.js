var request = require('request');
var fs = require('fs');
var directoryDriveLetter = process.env.NodeDirectoryDrive;
var services = process.env.M5ServiceLocation;
var DataCollector = {};

DataCollector.getData = function (data, callback) {
	var getDataUrl, SearchFilter;

	var billingMap = {
		Fee: ['0', '1', '2', '6', '7', '9', '11', '12', '13', '15', '16', '17', '18', '21', '22'],
		Summons: ['3', '8', '14', '19'],
		Parcel: ['20']
	}
	console.log('queryID', data.queryID);
	//Search Filters
	if (data.entity == 'Billing') {
		getDataUrl = services + '/Customer/' + data.customer + '/FilteredSearch';
		data.documentName = String(data.documentObject.Name).replace(/\W+/g, '');
		var dataType;
		for (i in billingMap) {
			if (billingMap[i].indexOf(data.queryID) != -1) {
				dataType = i;
				break;
			}
		}

		console.log('dataType', dataType);
		switch (dataType) {
			case 'Fee':
				SearchFilter = {
					"Base": "Billing",
					"Fields": [{
						"Fee": "FullName",
					}, {
						"Fee": "Address1"
					}, {
						"Fee": "Address2"
					},  {
						"Fee": "Address3"
					}, {
						"Fee": "Address4"
					}, {
						"Fee": "SBL"
					},{
						"Fee": "City"
					}, {
						"Fee": "State"
					}, {
						"Fee": "Amount"
					}, {
						"Fee": "PrintKey"
					}, {
						"Fee": "ZipCode"
					}, {
						"Fee": "InvoiceNumber"
					}, {
						"Fee": "InvoiceDate"
					}, {
						"Fee": "Date"
					}, {
						"Fee": "DueDate"
					}, {
						"Fee": "LegalAddress"
					}, {
						"Fee": "Fee_ID"
					}, {
						"Fee": "NumberOfYearsVacant"
					}, {
						"Fee": "ResidentialUnits"
					}, {
						"Fee": "BuildingConstructionType"
					}, {
						"Fee": "BoilerCount"
					}, {
						"Fee": "Section_ID"
					}, {
						"Fee": "PublicApplication_ID"
					}, {
						"Fee": "ParcelOwner"
					}, {
						"Fee": "Parcel_Owner_Name"
					}, {
						"Fee": "Parcel_Owner_Addr1"
					}, {
						"Fee": "Parcel_Owner_Addr2"
					}, {
						"Fee": "Parcel_Owner_City"
					}, {
						"Fee": "Parcel_Owner_State"
					}, {
						"Fee": "Parcel_Owner_Zip"
					}, {
						"Fee": "ReceiptBookNo"
					}, {
						"Fee": "ReceiptNo"
					}, {
						"Fee": "Comments"
					}, {
						"Fee": "Comments1"
					}, {
						"Fee": "PenaltyCalc"
					}, {
						"Fee": "PenaltyAmount"
					}, {
						"Fee": "PenaltyFee"
					}, {
						"Fee": "LegalAddr"
					}, {
						"Fee": "LegalAddrNo"
					}, {
						"Fee": "AssetName"
					}, {
						"Fee": "ControlNo"
					}, {
						"Fee": "Temp_Fee_ID"
					}, {
						"Fee": "InitialPenaltyAmount"
					}, {
						"Fee": "PenaltyCalc"
					}, {
						"Fee": "Bank"
					},{
						"Fee": "LastName"
					}, {
						"Billing": "Billing_ID"
					}],
					Relationships: {
						Children: [{
							Base: "Fee"
						}]
					},
					Filters: [{
						Entity: 'Billing',
						Name: 'Billing_ID',
						Operator: 'equals',
						Value: data.entity_ID
					}],
				};
				break;
			case 'Summons':
				SearchFilter = {
					"Base": "Billing",
					"Fields": [{
						"Summons": "Summons_ID",
					}, {
						"Summons": "ParcelOwner"
					}, {
						"Summons": "ParcelOwner_MailingAddressName"
					}, {
						"Summons": "ParcelOwner_MailingAddressAddr1"
					}, {
						"Summons": "ParcelOwner_MailingAddressAddr2"
					}, {
						"Summons": "ParcelOwner_MailingAddressCity"
					}, {
						"Summons": "ParcelOwner_MailingAddressState"
					}, {
						"Summons": "ParcelOwner_MailingAddressZip"
					}, {
						"Summons": "Number"
					}, {
						"Summons": "Disposition"
					}, {
						"Summons": "AppearanceCode"
					}, {
						"Summons": "DateWritten"
					}, {
						"Summons": "PrintKey"
					}, {
						"Summons": "LegalAddrNo"
					}, {
						"Summons": "LegalAddr"
					}],
					Relationships: {
						Children: [{
							Base: "Summons"
						}]
					},
					Filters: [{
						Entity: 'Billing',
						Name: 'Billing_ID',
						Operator: 'equals',
						Value: data.entity_ID
					}],
				};
				break;
			case 'License':
				SearchFilter = {
					"Base": "Billing",
					"Fields": [{
						"License": "License_ID",
					}, {
						"License": "Company_Addr1"
					}, {
						"License": "Company_Addr2"
					}, {
						"License": "Company_Name"
					}, {
						"License": "LicenseType"
					}, {
						"License": "LicenseNumber"
					}, {
						"License": "ExpirationDate"
					}, {
						"License": "RenewalNumber"
					}, {
						"License": "PolicyNumber"
					}, {
						"License": "City"
					}, {
						"License": "State"
					}, {
						"License": "Zip"
					}],
					Relationships: {
						Children: [{
							Base: "License"
						}]
					},
					Filters: [{
						Entity: 'Billing',
						Name: 'Billing_ID',
						Operator: 'equals',
						Value: data.entity_ID
					}],
				};
				break;
			case 'Parcel':
				SearchFilter = {
					"Base": "Billing",
					"Fields": [{
						"Parcel": "Parcel_ID",
					}, {
						"Parcel": "Owner1"
					}, {
						"Parcel": "LegalAddress"
					}, {
						"Parcel": "MailAddr1"
					}, {
						"Parcel": "MailAddr2"
					}, {
						"Parcel": "City"
					}, {
						"Parcel": "State"
					}, {
						"Parcel": "Zip"
					}, {
						"Parcel": "PrintKey"
					}],
					Relationships: {
						Children: [{
							Base: "Parcel"
						}]
					},
					Filters: [{
						Entity: 'Billing',
						Name: 'Billing_ID',
						Operator: 'equals',
						Value: data.entity_ID
					}],
				};
				break;
			default:
				break;
		}


	} else {
		getDataUrl = services + '/Customer/' + data.customer + '/' + data.entity + '/' + data.entity_ID;
		var extension = data.documentObject.URL.split('.').pop();
		data.documentName = String(data.documentObject.Name).replace(/\W+/g, '');
		SearchFilter = {
			"StartDate": data.startDate,
			"EndDate": data.endDate,
			"Tenant_ID": data.tenant_ID
		};
	}
	if (data.entity != 'SQLReport' && data.entity != 'Billing') {
		getDataUrl = services + '/Customer/' + data.customer + '/FilteredSearch';
		SearchFilter = {
			"Base": data.entity,
			"Fields": [{
				[data.entity]: '*'
			}],
			Filters: [{
				Name: data.entity + "_ID",
				Operator: 'equals',
				Value: data.entity_ID
			}]
		};
		// SearchFilter.Fields[entity]
	}
	console.log('getDataUrl', getDataUrl);
	console.log(SearchFilter);
	request({
		method: 'POST',
		url: getDataUrl,
		json: SearchFilter,
		headers: {
			'Authorization': 'Token ' + data.token
		},
		gzip: true,
	}, function (error, response, body) {
		var feeIDs = [];
		var fees = body.Data;

		SearchFilter = [];

		if (data.queryID == '0' || data.queryID == '13' || data.queryID == '12') {
			for (var i in fees) {

				if (fees[i].Fee_ID) {
					feeIDs.push(fees[i].Fee_ID);
					fees[i].Assets = [];
					SearchFilter.push({
						"Base": "Fee",
						"Fields": [{
							"Asset": "Name",
						}, {
							"Asset": "Description",
						}, {
							"Asset": "BoilersNumberOf",
						}, {
							"Fee": "Fee_ID"
						}],
						Relationships: {
							Siblings: [{
								Base: "Asset",
								Filters: [{
									Entity: 'Asset',
									Name: "Name",
									Operator: "equals",
									Value: "BOILER"
								}]
							}],
							Parents: [{
								Base: 'Occupant'
							}]
						},
						Filters: [{
							Entity: 'Fee',
							Name: 'Fee_ID',
							Operator: 'equals',
							Value: fees[i].Fee_ID
						}]
					});
				}
			}

			getDataUrl = services + '/Customer/' + data.customer + '/FilteredSearch';
			var totalAssets = [];
			var SearchFilterChunks = [];
			SearchFilterChunks[0] = [];
			var chunkCount = 0;
			var arrCount = 0;

			for (j in SearchFilter) {
				if (arrCount < 200) {
					SearchFilterChunks[chunkCount].push(SearchFilter[j]);
					arrCount++;
				} else {
					arrCount = 0;
					chunkCount++;
					SearchFilterChunks[chunkCount] = [];
					SearchFilterChunks[chunkCount].push(SearchFilter[j]);
					arrCount++;
				}
			}

			function servicesCallback(error, response, Assets) {
				totalAssets.push.apply(totalAssets, Assets);
				if (SearchFilterChunks.length > 0) {
					var myFilter = SearchFilterChunks.pop();
					request({
						method: 'POST',
						url: getDataUrl,
						json: myFilter,
						headers: {
							'Authorization': 'Token ' + data.token
						},
						gzip: true,
					}, servicesCallback);
				} else {
					if (totalAssets) {
						for (var x in totalAssets) {
							for (var y in fees) {
								if (fees[y].Fee_ID == totalAssets[x].Data[0].Fee_ID) {

									//Loop over asssets if length >0
									if (fees[y].Assets.length > 0) {
										for (var z in fees[y].Assets) {
											if (totalAssets[x].Data[0].Name == fees[y].Assets[z].Name) {
												fees[y].Asset[z].Count = Count++;
												fees[y].TotalCount = fees[y].TotalCount + fee[y].Asset[z].BoilersNumberOf;
												//if not then add new asset
											}
										}
										fees[y].Type = totalAssets[x].Data[0].Description;
									} else {
										totalAssets[x].Data[0].Count = 1;
										fees[y].TotalCount = totalAssets[x].Data[0].BoilersNumberOf;
										fees[y].Name = totalAssets[x].Data[0].Name;
										fees[y].Type = totalAssets[x].Data[0].Description;
										fees[y].Assets.push(totalAssets[x].Data[0]);
									}
								}
							}
						}
						fs.writeFile('./Fees.json', JSON.stringify(fees, null, 2), 'utf-8');
						callback({
							Data: fees
						}, dataType);
					} else {
						console.log('No  Assets');
					}
				}
			}

			request({
				method: 'POST',
				url: getDataUrl,
				json: SearchFilterChunks.pop(),
				headers: {
					'Authorization': 'Token ' + data.token
				},
				gzip: true,
			}, servicesCallback);

		} else if (data.queryID == '6' || data.queryID == '11') {
			var occIDs = [];
			var assetIDs = [];
			//console.log('Feeids',fees);
			for (var i in fees) {
				if (fees[i].Fee_ID) {
					feeIDs.push(fees[i].Fee_ID);
					assetIDs.push(fees[i].Section_ID);
					occIDs.push(fees[i].PublicApplication_ID);

					SearchFilter.push({
						"Base": "Fee",
						"Fields": [{
							"Asset": "*",
						}, {
							"Fee": "Fee_ID"
						}],
						Relationships: {
							Parents: [{
								Base: "Asset"
							}]
						},
						Filters: [{
							Entity: 'Fee',
							Name: 'Fee_ID',
							Operator: 'equals',
							Value: fees[i].Fee_ID
						}],
					});
				}
			}
			console.log('Feeids', feeIDs);

			getDataUrl = services + '/Customer/' + data.customer + '/FilteredSearch';
			var totalAssets = [];
			var SearchFilterChunks = [];
			SearchFilterChunks[0] = [];
			var chunkCount = 0;
			var arrCount = 0;

			for (j in SearchFilter) {
				if (arrCount < 200) {
					SearchFilterChunks[chunkCount].push(SearchFilter[j]);
					arrCount++;
				} else {
					console.log('Bigger');
					arrCount = 0;
					chunkCount++;
					SearchFilterChunks[chunkCount] = [];
					SearchFilterChunks[chunkCount].push(SearchFilter[j]);
					arrCount++;
				}
			}

			// Put the callback in a function
			// Need to make multiple calls for Signs
			function servicesCallback(error, response, Assets) {
				totalAssets.push.apply(totalAssets, Assets);
				if (SearchFilterChunks.length > 0) {
					var myFilter = SearchFilterChunks.pop();
					//fs.appendFile('D:\\Municity Media\\JTF.txt', JSON.stringify(Assets) + '\\r\\n' + '===============' + '\\r\\n', 'utf-8');
					console.log(getDataUrl, myFilter.length);
					request({
						method: 'POST',
						url: getDataUrl,
						json: myFilter,
						headers: {
							'Authorization': 'Token ' + data.token
						},
						gzip: true,
					}, servicesCallback);
				} else {
					var newData = [];
					var dataMap = {};

					console.log('How many assets we got? ', totalAssets.length);
					if (totalAssets) {
						for (var x in totalAssets) {
							for (var y in fees) {
								//console.log(fees[y].Fee_ID);
								//console.log(totalAssets[x]);
								//console.log(myFilter);
								if (fees[y].Fee_ID == totalAssets[x].Data[0].Fee_ID) {
									// Check if there's already stuff on the occupant
									if (dataMap.hasOwnProperty(fees[y].PublicApplication_ID)) {
										dataMap[fees[y].PublicApplication_ID].TotalAmount = parseFloat(dataMap[fees[y].PublicApplication_ID].TotalAmount) + parseFloat(fees[y].Amount);
										dataMap[fees[y].PublicApplication_ID].SignCount = dataMap[fees[y].PublicApplication_ID].SignCount + 1;
										totalAssets[x].Data[0]["Amount"] = fees[y].Amount;
										dataMap[fees[y].PublicApplication_ID].Signs.push(totalAssets[x].Data[0]);
									}
									// If not, create a new entry in the dataMap for the Occupant
									else {
										var obj = fees[y];
										obj["TotalAmount"] = parseFloat(fees[y].Amount);
										obj["SignCount"] = 1;
										obj["Signs"] = [];
										obj["Occupant_ID"] = fees[y].PublicApplication_ID;
										totalAssets[x].Data[0]["Amount"] = fees[y].Amount;
										obj["Signs"].push(totalAssets[x].Data[0]);
										dataMap[fees[y].PublicApplication_ID] = obj;
									}
								}
							}

						}

						for (z in dataMap) {
							dataMap[z].TotalAmount = dataMap[z].TotalAmount.toFixed(2);
							newData.push(dataMap[z]);
						}

						dataType = "Occupant";
						console.log('Number of Occupants', newData.length);
						callback({
							Data: newData
						}, dataType);
					} else {
						console.log('No  Assets');
					}
				}
			}

			request({
				method: 'POST',
				url: getDataUrl,
				json: SearchFilterChunks.pop(),
				headers: {
					'Authorization': 'Token ' + data.token
				},
				gzip: true,
			}, servicesCallback);

			// For Elevators
		} else if (data.queryID == '1' || data.queryID == '16') {
			var newData = [];
			var dataMap = {};

			for (var i in fees) {
				if (dataMap.hasOwnProperty(fees[i].Section_ID)) {
					dataMap[fees[i].Section_ID].TotalAmount = parseFloat(dataMap[fees[i].Section_ID].TotalAmount) + parseFloat(fees[i].Amount);
					dataMap[fees[i].Section_ID].TotalCount = dataMap[fees[i].Section_ID].TotalCount + 1;
					dataMap[fees[i].Section_ID].Assets.push(fees[i]);
				} else {
					var obj = fees[i];
					obj["TotalAmount"] = parseFloat(fees[i].Amount);
					obj["TotalCount"] = 1;
					obj["Assets"] = [];
					obj["Occupant_ID"] = fees[i].Section_ID;
					obj["Assets"].push(fees[i]);
					dataMap[fees[i].Section_ID] = obj;
				}
			}

			for (z in dataMap) {
				dataMap[z].TotalAmount = dataMap[z].TotalAmount.toFixed(2);
				newData.push(dataMap[z]);
			}

			dataType = "Occupant";
			console.log('Number of Occupants', newData.length);
			callback({
				Data: newData
			}, dataType);
			// For Elevator Late Fees -- Elevator fees are all on the Occupant, and could be grouped by the ids
			// In this case, we need to get the original fees to group by for the printout 
		} else if (data.queryID == '9') {
			//The original fees for the elevator query were not generated by the billing system
			//So sorry, but I'm using the temp_fee_id to find them on their occupants
			//The temp_fee_id is used in the Elevator Permit SQL query as well - 5/24/2018
			//fs.appendFile('D:\\Municity Media\\JTF.txt', JSON.stringify(fees) + '\\r\\n' + '===============' + '\\r\\n', 'utf-8');
			for (var i in fees) {

				if (fees[i].Fee_ID) {
					feeIDs.push(fees[i].Fee_ID);
					fees[i].Assets = [];
					SearchFilter.push({
						"Base": "Fee",
						"Fields": [{
							"Fee": "Comments1"
						}, {
							"Fee": "Fee_ID"
						}, {
							"Fee": "Comments"
						}, {
							"Fee": "ControlNo"
						}, {
							"Fee": "Amount"
						}, {
							"Fee": "InvoiceNumber"
						}, {
							"Occupant": "Occupant_ID"
						}],
						Relationships: {
							Parents: [{
								Base: 'Occupant',
								Filters: [{
									"Entity": "Occupant",
									"Name": "Occupant_ID",
									"Operator": "equals",
									"Value": fees[i].PublicApplication_ID
								}]
							}]
						},
						Filters: [{
							Entity: 'Fee',
							Name: 'Temp_Fee_ID',
							Operator: 'equals',
							Value: '537391162017'
						}],
					});
				}
			}

			//fs.appendFile('D:\\Municity Media\\JTF.txt', JSON.stringify(SearchFilter) + '\\r\\n' + '===============' + '\\r\\n', 'utf-8');
			getDataUrl = services + '/Customer/' + data.customer + '/FilteredSearch';
			request({
				method: 'POST',
				url: getDataUrl,
				json: SearchFilter,
				headers: {
					'Authorization': 'Token ' + data.token
				},
				gzip: true,
			}, function (error, response, Assets) {
				//In this case, Assets are really just the original fees
				if (Assets) {
					for (var x in Assets) {
						for (var y in fees) {
							if (Assets[x].Data[0]) {
								if (fees[y].PublicApplication_ID == Assets[x].Data[0].Occupant_ID) {
									for (var j in Assets[x].Data) {
										if (fees[y].Assets.length > 0) {
											fees[y]["TotalCount"] = fees[y]["TotalCount"] + 1
											fees[y]["TotalAmount"] = fees[y]["TotalAmount"] + parseFloat(Assets[x].Data[j].Amount);
											fees[y].Assets.push(Assets[x].Data[j]);
											//Zero Case
											//Initialize the TotalCount and TotalAmount fields on the late fee
											//Also add original InvoiceNumber and Legal Addr fields
										} else {
											fees[y]["TotalCount"] = 1;
											fees[y]["TotalAmount"] = parseFloat(Assets[x].Data[j].Amount);
											fees[y]["OriginalInvoiceNumber"] = Assets[x].Data[j].InvoiceNumber;
											fees[y].Assets.push(Assets[x].Data[j]);
										}
									}
								}
							}
						}
					}

					for (var z in fees) {
						fees[z].Amount = parseFloat(fees[z].Amount) + parseFloat(fees[z].TotalAmount);
						fees[z].Amount = parseFloat(fees[z].Amount).toFixed(2);
					}

					fs.writeFile('./Fees.json', JSON.stringify(fees, null, 2), 'utf-8');
					callback({
						Data: fees
					}, dataType);
				} else {
					console.log('No late fees');
				}
			});
		} else if (data.queryID == '15') {
			/**
			 * For Vacant Building Late Fees
			 * the original Fee_ID is in the Temp_Fee_ID Column of the late fee
			 */
			for (var i in fees) {
				if (fees[i].Fee_ID) {
					feeIDs.push(fees[i].Fee_ID);
					SearchFilter.push({
						"Base": "Fee",
						"Fields": [{
							"Fee": "Comments1"
						}, {
							"Fee": "Fee_ID"
						}, {
							"Fee": "Comments"
						}, {
							"Fee": "ControlNo"
						}, {
							"Fee": "Amount"
						}, {
							"Fee": "InvoiceNumber"
						}, {
							"Fee": "NumberOfYearsVacant"
						}, {
							"Fee": "ResidentialUnits"
						}],
						Relationships: {
							Parents: [],
							Children: [],
							Siblings: []
						},
						Filters: [{
							Entity: 'Fee',
							Name: 'InvoiceNumber',
							Operator: 'equals',
							Value: fees[i].InvoiceNumber
						}, {
							Entity: 'Fee',
							Name: 'FeeType',
							Operator: 'equals',
							Value: 'VACANT BUILDING'
						}],
					});
				}
			}

			var totalOGFees = [];
			var SearchFilterChunks = [];
			SearchFilterChunks[0] = [];
			var chunkCount = 0;
			var arrCount = 0;

			//Of course this call times out
			//Chunk thing again
			for (j in SearchFilter) {
				if (arrCount < 50) {
					SearchFilterChunks[chunkCount].push(SearchFilter[j]);
					arrCount++;
				} else {
					arrCount = 0;
					chunkCount++;
					SearchFilterChunks[chunkCount] = [];
					SearchFilterChunks[chunkCount].push(SearchFilter[j]);
					arrCount++;
				}
			}

			getDataUrl = services + '/Customer/' + data.customer + '/FilteredSearch';

			function servicesCallback(error, response, OGFees) {
				totalOGFees.push.apply(totalOGFees, OGFees);
				if (SearchFilterChunks.length > 0) {
					var myFilter = SearchFilterChunks.pop();
					request({
						method: 'POST',
						url: getDataUrl,
						json: myFilter,
						headers: {
							'Authorization': 'Token ' + data.token
						},
						gzip: true,
					}, servicesCallback);
				} else {
					if (totalOGFees) {
						for (var x in totalOGFees) {
							for (var y in fees) {
								if (totalOGFees[x].Data[0]) {
									if (fees[y].InvoiceNumber == totalOGFees[x].Data[0].InvoiceNumber) {
										fees[y]["OriginalAmount"] = totalOGFees[x].Data[0].Amount;
										fees[y]["TotalAmount"] = parseFloat(fees[y].Amount) + parseFloat(totalOGFees[x].Data[0].Amount);
										fees[y]["TotalAmount"] = parseFloat(fees[y]["TotalAmount"]).toFixed(2);
										fees[y]["ResidentialUnits"] = totalOGFees[x].Data[0].ResidentialUnits;
										fees[y]["NumberOfYearsVacant"] = totalOGFees[x].Data[0].NumberOfYearsVacant;
									}
								}
							}
						}

						fs.writeFile('./Fees.json', JSON.stringify(fees, null, 2), 'utf-8');
						callback({
							Data: fees
						}, dataType);
					} else {
						console.log('No late fees');
					}
				}
			}

			request({
				method: 'POST',
				url: getDataUrl,
				json: SearchFilterChunks.pop(),
				headers: {
					'Authorization': 'Token ' + data.token
				},
				gzip: true,
			}, servicesCallback);
		} else if (data.queryID == '7') {
			for (var i in fees) {
				if (fees[i].Fee_ID) {
					SearchFilter.push({
						"Base": "Occupant",
						"Fields": [{
							"Asset": "Asset_ID",
						}, {
							"Fee": "Fee_ID"
						}, {
							"Fee": "InvoiceNumber"
						}, {
							"Fee": "Amount"
						}],
						Relationships: {
							Parents: [{
								Base: "Asset"
							}]
						},
						Filters: [{
							Entity: 'Fee',
							Name: 'Fee_ID',
							Operator: 'equals',
							Value: fees[i].Fee_ID
						}],
					});
				}
			}


			getDataUrl = services + '/Customer/' + data.customer + '/FilteredSearch';
			var totalAssets = [];
			var SearchFilterChunks = [];
			SearchFilterChunks[0] = [];
			var chunkCount = 0;
			var arrCount = 0;

			for (j in SearchFilter) {
				if (arrCount < 200) {
					SearchFilterChunks[chunkCount].push(SearchFilter[j]);
					arrCount++;
				} else {
					arrCount = 0;
					chunkCount++;
					SearchFilterChunks[chunkCount] = [];
					SearchFilterChunks[chunkCount].push(SearchFilter[j]);
					arrCount++;
				}
			}

			// Put the callback in a function
			// Need to make multiple calls for Signs
			function servicesCallback(error, response, Assets) {
				totalAssets.push.apply(totalAssets, Assets);
				if (SearchFilterChunks.length > 0) {
					var myFilter = SearchFilterChunks.pop();
					//fs.appendFile('D:\\Municity Media\\JTF.txt', JSON.stringify(Assets) + '\\r\\n' + '===============' + '\\r\\n', 'utf-8');
					console.log(getDataUrl, myFilter.length);
					request({
						method: 'POST',
						url: getDataUrl,
						json: myFilter,
						headers: {
							'Authorization': 'Token ' + data.token
						},
						gzip: true,
					}, servicesCallback);
				} else {
					var newData = [];
					var dataMap = {};

					console.log('How many assets we got? ', totalAssets.length);
					if (totalAssets) {
						for (var x in totalAssets) {
							for (var y in fees) {
								if (fees[y].Fee_ID == totalAssets[x].Data[0].Fee_ID) {
									// Check if there's already stuff on the occupant
									if (dataMap.hasOwnProperty(fees[y].PublicApplication_ID)) {
										dataMap[fees[y].PublicApplication_ID].TotalAmount = parseFloat(dataMap[fees[y].PublicApplication_ID].TotalAmount) + parseFloat(fees[y].Amount);
										dataMap[fees[y].PublicApplication_ID].SignCount = dataMap[fees[y].PublicApplication_ID].SignCount + 1;
										totalAssets[x].Data[0]["Amount"] = fees[y].Amount;
										dataMap[fees[y].PublicApplication_ID].Signs.push(totalAssets[x].Data[0]);
									}
									// If not, create a new entry in the dataMap for the Occupant
									else {
										var obj = fees[y];
										obj["TotalAmount"] = parseFloat(fees[y].Amount);
										obj["SignCount"] = 1;
										obj["Signs"] = [];
										obj["Occupant_ID"] = fees[y].PublicApplication_ID;
										totalAssets[x].Data[0]["Amount"] = fees[y].Amount;
										obj["Signs"].push(totalAssets[x].Data[0]);
										dataMap[fees[y].PublicApplication_ID] = obj;
									}
								}
							}

						}

						for (z in dataMap) {
							dataMap[z].TotalAmount = dataMap[z].TotalAmount.toFixed(2);
							newData.push(dataMap[z]);
						}

						dataType = "Occupant";
						console.log('Number of Occupants', newData.length);
						callback({
							Data: newData
						}, dataType);
					} else {
						console.log('No  Assets');
					}
				}
			}

			request({
				method: 'POST',
				url: getDataUrl,
				json: SearchFilterChunks.pop(),
				headers: {
					'Authorization': 'Token ' + data.token
				},
				gzip: true,
			}, servicesCallback);

		} else {
			console.log('here', fees);
			callback({
				Data: fees
			}, dataType);
		}

	});
}

/**
 *@desc Look up a specific record.
 *@param {string} entity The type of entity to look up.
 *@param {int} entityId The ID to look up.
 *@param {string} token Token for authorization.
 *@param {string} customer Which tenant the record belongs to.
 *@param {function} callback Callback function, returns success and a response
 *                           object.
 */
DataCollector.getEntityData = function (entity, entityId, token, customer, callback) {
	var getDataUrl = services + '/Customer/' + customer + '/FilteredSearch';
	var searchFilter = {
		"Base": entity,
		"Fields": [{
			[entity]: '*'
		}],
		Filters: [{
			Name: entity + "_ID",
			Operator: 'equals',
			Value: entityId
		}]
	}

	request({
		method: 'POST',
		url: getDataUrl,
		json: searchFilter,
		headers: {
			'Authorization': 'Token ' + token
		},
		gzip: true,
	}, function (error, response, body) {
		if (!error && response.statusCode == 200) {
			callback(true, body.Data[0]);
		} else {
			console.log('Filtered Search failed');
			console.log(getDataUrl);
			console.log('status code: ' + response.statusCode);
			console.log(error);
			callback(false);
		}
	});
};

/**
 *@desc Look up a specific record.
 *@param {string} entity The type of entity to look up children for.
 *@param {int} entityId The ID to look up children for.
 *@param {string} token Token for authorization.
 *@param {string} customer Which tenant the parent record belongs to.
 *@param {function} callback Callback function, returns success and a response
 *                           object.
 */
DataCollector.getChildrenData = function (entity, entityId, token, customer, callback) {
	var getChildrenUrl = services + '/Customer/' + customer;
	getChildrenUrl += '/' + entity + '/' + entityId;
	getChildrenUrl += '/Children';

	request({
		method: 'GET',
		url: getChildrenUrl,
		headers: {
			'Authorization': 'Token ' + token
		},
		gzip: true,
	}, function (error, response, body) {
		if (!error && response.statusCode == 200) {
			var res = body;
			if (body) {
				res = JSON.parse(body);
			}
			callback(true, res);
		} else {
			console.log('Child lookup failed');
			console.log(getChildrenUrl);
			console.log('status code: ' + response.statusCode);
			console.log(error);
			callback(false);
		}
	});
};

/**
 *@desc Get breadcrumbs for a record. Useful for finding parent information.
 *@param {string} entity The type of entity to look up children for.
 *@param {int} entityId The ID to look up children for.
 *@param {string} token Token for authorization.
 *@param {string} customer Which tenant the parent record belongs to.
 *@param {function} callback Callback function, returns success and a response
 *                           object.
 */
DataCollector.getBreadcrumbs = function (entity, entityId, token, customer, callback) {
	var breadcrumbUrl = services + '/M5BreadCrumbs';
	breadcrumbUrl += '?customer=' + customer;
	breadcrumbUrl += '&Entity=' + entity;
	breadcrumbUrl += '&EntityID=' + entityId;

	request({
		method: 'GET',
		url: breadcrumbUrl,
		headers: {
			'Authorization': 'Token ' + token
		},
		gzip: true,
	}, function (error, response, body) {
		if (!error && response.statusCode == 200) {
			var res = JSON.parse(body);
			if (res.rows) {
				res = res.rows;
			} else {
				res = [];
			}
			callback(true, res);
		} else {
			console.log('Breadcrumb lookup failed');
			console.log(breadcrumbUrl);
			console.log('status code: ' + response.statusCode);
			console.log(error);
			callback(false, error);
		}
	});
};

/**
 *@desc Get parent data for a record. Calls getBreadcrumbs to get a list of
 *			 entities to look up.
 *@param {string} entity The type of the child entity to find a parent for.
 *@param {int} entityId The ID of the child entity.
 *@param {string} token Token for authorization.
 *@param {string} customer Which tenant to search in.
 *@param {function} callback Callback function, returns success and a response
 *                           object.
 */
DataCollector.getParentData = function (entity, entityId, token, customer, callback) {
	DataCollector.getBreadcrumbs(entity, entityId, token, customer, function (success, breadcrumbResponse) {
		if (success) {
			DataCollector.getParentDataHelper(breadcrumbResponse, token, customer, callback);
		} else {
			callback(false, breadcrumbResponse);
		}
	});
};

DataCollector.getParentDataHelper = function (parentArray, token, customer, callback, index, parentObject) {
	if (typeof index !== 'number') {
		index = parentArray.length - 2; //First result is always the current item, so we can skip that
	}
	if (!parentObject) {
		parentObject = {};
	}

	var item = parentArray[index];
	if (item) {
		DataCollector.getEntityData(item.Entity, item.Id, token, customer, function (success, parentData) {
			if (success) {
				parentObject[item.Entity] = parentData;
				DataCollector.getParentDataHelper(parentArray, token, customer, callback, index - 1, parentObject);
			} else {
				callback(false, parentData);
			}
		});
	} else {
		callback(true, parentObject);
	}
};

/**
 *@desc Get the customer info record
 *@param {object} data Data object containing a customer attribute that should
 *                     be the tenant name
 *@param {function} callback Callback function, returns success and a response
 *                           object.
 **/
DataCollector.getCustomerInfoData = function (customer, callback) {
	var customerInfoUrl = services + '/Customer/' + customer + '/CustomerInfo';

	request({
		method: 'GET',
		json: true,
		url: customerInfoUrl,
		gzip: true
	}, function (error, response, body) {
		if (!error && response.statusCode == 200) {
			callback(true, body);
		} else {
			console.log('CustomerInfo lookup failed');
			console.log(customerInfoUrl);
			console.log('status code: ' + response.statusCode);
			console.log(error);
			callback(false);
		}
	});
};

module.exports = DataCollector;