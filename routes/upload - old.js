//========================================================
// Municity Node
// File Upload
//
// Prefix:              /upload
// Currently Supports:  /
//
// Author:          Caleb Wright <cwright@sca-corp.com>
// Created Date:    5/9/2014
// Last Modified:   5/12/2014
//========================================================

var express 	= require('express');
var router 		= express.Router();
var fs			= require('fs');
var nodefs		= require('node-fs');
var formidable 	= require('formidable');
var im 			= require('imagemagick');
var child_process = require('child_process');

var mediaDirectory = 'D:/Municity Media/';

var quarantineContentTypes = [
	"application/x-msdownload",
	"application/javascript"
];

var thumbnailTypes = [
	"image/jpeg",
	"image/png",
	"image/bmp",
	"image/tiff"
];


var createPdfThumb = function(inputPath, outputPath, logStream){
	var ls = child_process.spawn('C:/Program Files/gs/gs9.18/bin/gswin64c.exe', ['-sDEVICE=png16', '-r96', '-dFirstPage=1', '-dLastPage=1', '-o', outputPath, inputPath])		
	logStream.write('Attempting Thumbnail Creation - Input: ' + inputPath + ' Output: ' + outputPath + '\r\n');
	ls.on('error', function(err){
		logStream.write('Error Attempting Thumbnail Creation - Input: ' + inputPath + ' Output: ' + outputPath + ' Error: ' + err + '\r\n');
	})
}

router.post('/', function(req, res) {

	var form 				= new formidable.IncomingForm();
	var filePath 			= '';
	var fileName 			= '';
	var fileType 			= '';
	var rn 					= Math.floor(Math.random()*1000000000);
	//SEND TO QUARANTINE DEFAULT TO FALSE (CHECKS CONTENT TYPE ON FILE BEGIN)
	var sendToQuarantine 	= false;

	form.encoding 		= 'utf-8';
	form.uploadDir 		= mediaDirectory;
	form.keepExtensions = true;
	form.hash 			= false;

	//FIELDS OTHER THAN FILES - IN THIS CASE THE PATH
	form.on('field', function(name, value){
		//CHECK FOR PATH PARAMETER
		if(name.toLowerCase() === 'path'){
			filePath = value;
			//req.logStream.write('File Path: ' + filePath + '\r\n');
		}
	});
	//HANDLE FORM ERROR
	form.on('error', function(err) {
		//req.logStream.write('Upload Form Error' + '\r\n' + err + '\r\n');
	    res.send({Success:false, Response:'Upload Form Error'});
	});
	//EMITTED WHEN A FILE HITS THE UPLOAD STREAM
	form.on('file', function(name, file) {
	
	});
	//EMITTED WHEN A FILE IS DETECTED
	form.on('fileBegin', function(field, file) {
		//GET FILE NAME AND SET DESTINATION PATH
	    fileName 	= file.name;
	    fileType	= file.type;
	    file.path 	= mediaDirectory + 'tmp/' + rn + fileName;
	    //CHECK AGAINST QUARANTINE CONTENT TYPES FOR ACCEPTED FILE
	    for(var x = 0; x < quarantineContentTypes.length; x++){
	    	//IF INVALID, SEND TO QUARANTINE
	    	if(file.type == quarantineContentTypes[x]){
	    		file.path 			= mediaDirectory + 'Quarantine/' + fileName;
	    		sendToQuarantine 	= true;
	    	}
	    }
	    
	});
	//EMITTED ON EVERY CHUNK OF DATA - CAN BE USED TO ROLL A PROGRESS BAR? 
	form.on('progress', function(bytesReceived, bytesExpected) {
		
	});
	//ENTIRE REQUEST RECEIVED AND FILES HAVE FINISHED FLUSHING TO DISK
	form.on('end', function() {
		if(sendToQuarantine){
			//UNLINK REMOVES FILES
			fs.unlink(mediaDirectory + 'Quarantine/' + fileName, function(err){
				if(err){
					//req.logStream.write('Quarantine Delete Failed: ' + mediaDirectory + 'Quarantine/' + fileName + '\r\n' + err + '\r\n');
		    		res.send({Success:false, Response:'Unaccepted File Type'});
	    		}else{
	    			//req.logStream.write('Quarantined and Deleted File: ' + mediaDirectory + 'Quarantine/' + fileName + '\r\n');
		    		res.send({Success:false, Response:'Unaccepted File Type'});
	    		}
			});
		}else{
			//CHECK PATH EXISTS AND MOVE UPLOAD INTO IT
			nodefs.mkdir(mediaDirectory + filePath, 0777, true, function(err){
	    		if(err){
	    			//req.logStream.write('Error Creating Directory: ' + mediaDirectory + filePath + '\r\n' + err + '\r\n');
	    			res.send({Success:false, Response:'Failed to Create Directory'});
	    		}else{
	    			nodefs.mkdir(mediaDirectory + 'Thumbnails/' + filePath, 0777, true, function(err){
			    		if(err){
			    			//req.logStream.write('Error Creating Directory: ' + mediaDirectory + 'Thumbnails/' + filePath + '\r\n' + err + '\r\n');
			    			res.send({Success:false, Response:'Failed to Create Directory'});
			    		}else{
			    			fs.rename(mediaDirectory + 'tmp/' + rn + fileName, mediaDirectory +  filePath + '/' + fileName, function(err){
								if(err){
									//req.logStream.write('Error Moving File: ' + mediaDirectory + fileName + '\r\n' + err + '\r\n');
									res.send({Success:false, Response:'Failed to Move File to Path'});
								}else{
									for(var x = 0; x < thumbnailTypes.length; x++){
										if(fileType == thumbnailTypes[x]){
											try{
												im.convert([mediaDirectory + filePath + '/' + fileName, '-resize','200x200', mediaDirectory + 'Thumbnails/' + filePath + '/' + fileName],
												function(err, stdout){
													if(err){
														//req.logStream.write('Error Creating Thumbnail' + err + '\r\n');
													}
												});
											}catch(error){
												//req.logStream.write('Error Attempting Thumbnail Creation' + error + '\r\n');
						    				}
										}
									}
									if(fileType == 'application/pdf'){
										createPdfThumb(mediaDirectory + filePath + '/' + fileName, mediaDirectory + 'Thumbnails/' + filePath + '/' + fileName.replace(/pdf$/, 'png'), req.logStream);
									}
			    					res.send({Success:true, Response:'File Uploaded Successfully'});
			    				}
			    			});
						}
	    			});
				}
			});
		}
	});
	//PARSES INCOMING FORM, FIRES CALLBACK WITH ALL FIELDS AND FILES
	form.parse(req, function(err, fields, files) {
		if(err){
			//req.logStream.write('Error Parsing Form' + '\r\n' + err + '\r\n');
		    res.send({Success:false, Response:'Error Parsing Form'});
		}
	});

	
});

module.exports = router;