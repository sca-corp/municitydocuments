//=================================================================================
// Municity Node
// Document Generator
//
// Author:          Marcus Allen <MarcusA@Sca-corp.com>
// Created Date:    11/13/2017
// Last Modified:   11/21/2017
//=================================================================================
var JSZip = require('jszip');
var Docxtemplater = require('docxtemplater');
var ImageModule = require('docxtemplater-image-module');
var path = require('path');
var request = require('request');
var moment = require('moment');
var express = require('express');
var fs = require('fs');
var router = express.Router();
var mkdirp = require('mkdirp');
var municity = require('../init.js');
var XlsxTemplate = require('xlsx-template');
var dataCollector = require('../routes/dataCollector.js');
var dataExtractor = require('../routes/dataExtractor.js');
var directoryDriveLetter = process.env.NodeDirectoryDrive;
var services = process.env.M5ServiceLocation;
var mediaDirectory = directoryDriveLetter + '/Municity Media';
var loggerApiUrl = 'http://production.ec5pxxzzz3.us-east-1.elasticbeanstalk.com/api';
var QRCode = require('qrcode');
var debugMode = true;

//To help with pluralizing entity names
var pluralize = require('pluralize');

// | functions for modifying data in word docs.
var expressions = require('angular-expressions');

expressions.filters.size = function (input, width, height) {
	return {
		data: input,
		size: [width, height],
	};
};

expressions.filters.QR = function (tag) {
	console.log('tag', tag);
	//tag.chicken=1;
	return 'QRGENERATE';
};

expressions.filters.raw = function (text) {
	if (!text) {
		return text;
	}
	text = escapeXml(text);
	var lines = text.split("\n");
	var pre = "<w:p><w:r><w:t>";
	var post = "</w:t></w:r></w:p>";
	var lineBreak = "<w:br/>";
	return pre + lines.join(lineBreak) + post;
};
expressions.filters.date = function (text) {
	if (!text) {
		return text;
	}

	text = escapeXml(text);
	var formatted = moment(text).format('MM/DD/YYYY');
	return formatted;
};
expressions.filters.yyyy = function (text) {
	if (!text) {
		return text;
	}

	text = escapeXml(text);
	var formatted = moment(text).format('YYYY');
	return formatted;
};
expressions.filters.checkbox = function (text) {
	if (text == undefined || text == null) {
		return text;
	}

	//text = escapeXml(text);

	if (text == '1' || text == true || text == 1 || text == 'Yes' || text == 'yes') {
		return "☑";
	} else {
		return "☐";
	}
};
expressions.filters.yesno = function (text) {
	if (text == undefined || text == null) {
		return text;
	}

	//text = escapeXml(text);

	if (text == '1' || text == true || text == 1 || text == 'Yes' || text == 'yes') {
		return "Yes";
	} else {
		return "No";
	}
};
expressions.filters.lowercase = function (text) {
	if (text == undefined || text == null) {
		return text;
	}

	// text = escapeXml(text);
	var formatted = text.toLowerCase();
	return formatted;
};
expressions.filters.uppcase = function (text) {
	if (text == undefined || text == null) {
		return text;
	}

	//text = escapeXml(text);
	var formatted = text.toUpperCase();
	return formatted;
};
expressions.filters.currency = function (text) {
	if (text == undefined || text == null) {
		return text;
	}

	text = escapeXml(text);
	var formatted = Number(text);
	if (formatted) {
		formatted = formatted.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}
	return formatted;
};

angularParser = function (tag) {
	f = expressions.compile(tag);
	return {
		get: function (scope) {
			if (f(scope) === undefined || f(scope) === 'undefined' || f(scope) === null)
				return "";
			else return f(scope);
		}
	};
};

router.route('/DebugMode').get(function (req, res) {
	res.send({
		Success: true,
		DebugMode: debugMode
	});
});

router.route('/ToggleDebugMode').post(function (req, res) {
	if (req.body.debugMode !== undefined) {
		debugMode = Boolean(req.body.debugMode);
	} else {
		debugMode = !debugMode;
	}
	res.send({
		Success: true,
		DebugMode: debugMode
	});
});

router.route('/TestBigData').post(function (req, res) {
	res.send({
		Success: true,
		BigData: req.body.BigData
	});
});

//Routes
router.route('/:Customer/:Entity/:Entity_ID/Document/:Document_ID/Merge').get(function (req, res) {
	var data = {
		customer: req.params.Customer,
		document_ID: req.params.Document_ID,
		entity: req.params.Entity,
		entity_ID: req.params.Entity_ID,
		mailmerge: true,
		startDate: req.query.startdate,
		endDate: req.query.enddate,
		queryID: req.query.query_ID,
		tenant_ID: req.query.Tenant_ID,
		excel: false,
		origReq: req,
		origRes: res,
		callback: generateDocument
	};
	loadCustomerInfoRecord(data);
});

router.route('/:Customer/SQLReport/:SQLReport_ID/Document/:Document_ID').get(function (req, res) {
	console.log('In sqlReport route');
	var callId = createGUID();

	var customer = req.params.Customer;
	var entity = 'SQLReport';
	var entityId = req.params.SQLReport_ID;
	var documentId = req.params.Document_ID;

	if (debugMode) {
		console.log(callId + ' ' + customer + ' - Document ' + documentId + ' - ' + entity + ' ' + entityId + ' - Create SQLReport');
	}

	var data = {
		customer: customer,
		document_ID: documentId,
		entity: entity,
		entity_ID: entityId,
		mailmerge: false,
		startDate: req.query.startdate,
		endDate: req.query.enddate,
		tenant_ID: req.query.Tenant_ID,
		excel: false,
		origReq: req,
		origRes: res,
		callback: generateDocument
	};
	loadCustomerInfoRecord(data);
});

router.route('/:Customer/:Entity/:Entity_ID/Document/:Document_ID').get(function (req, res) {

	var callId = createGUID();
	var customer = req.params.Customer;
	var entity = req.params.Entity;
	var entityId = req.params.Entity_ID;
	var documentId = req.params.Document_ID;
	var fileType = req.query.FileType;
	var token = req.query.token;
	var contactId = req.query.Contact_ID;
	var username = req.query.Username;
	var useCustomSQL = req.query.UseCustomSQL;
	var saveLocation = mediaDirectory + '/' + customer + '/' + entity + '/' + entityId;

	var loggingObj = {
		contactId: contactId ? Number(contactId) : null,
		documentId: documentId,
		entity: entity,
		entityId: entityId,
		guid: callId,
		tenantName: customer,
		success: false
	};

	if (debugMode) {
		console.log(callId + ' ' + customer + ' - Document ' + documentId + ' - ' + entity + ' ' + entityId + ' - Create Word Document');
	}

	//Get document template record from the database
	getDocumentTemplate(customer, documentId, function (success, documentTemplate) {
		if (success) {
			loggingObj.name = documentTemplate.Name;

			var templatePath = mediaDirectory + '/' + documentTemplate.URL.replaceAll('/', '\\');

			if (useCustomSQL == 'true') {
				//Get data from CustomSQL Endpoint if Custom SQL
				var DocumentSQLURL = services;
				DocumentSQLURL += '/Customer/' + customer + '/';
				DocumentSQLURL += 'DocumentTemplate/' + documentId + '/';
				DocumentSQLURL += entity + '/' + entityId + '?useNew=true';
				if(contactId){
					DocumentSQLURL += '&contactid='+ contactId;
				}
				var documentData = '';


				console.log(DocumentSQLURL);
				request({
						method: 'GET',
						url: DocumentSQLURL,
						gzip: true,
						json: true,
						headers: {
							'X-Requested-With': 'XMLHttpRequest'
						}
					},
					function (error, response, body) {
						documentData = body;

						var generateDocumentFunction = generateWordDocument;


						if (fileType) {
							if (fileType.toLowerCase() === 'Excel') {
								generateDocumentFunction = generateExcelDocument;
							}
						}

						generateDocumentFunction(templatePath, documentData, function (success, documentResponse) {
							console.log('generateDocumentFunction documentId', documentId)
							loggingObj.fileType = documentResponse.fileType;
							if (success) {
								//Save it
								var fileName = String(documentTemplate.Name).replace(/\W+/g, '');
								saveFile(
									documentResponse,
									fileName,
									saveLocation,
									customer,
									entity,
									entityId,
									token,
									function (success, saveFileResponse) {
										if (success) {
											loggingObj.url = saveFileResponse.Url;
											loggingObj.success = true;
											loggingObj.responseMessage = 'Success';
											documentData.SaveFileResponse = saveFileResponse;
											sendResponse(res, documentData, loggingObj);
										} else {
											loggingObj.responseMessage = 'Failed to save document.';
											sendResponse(res, saveFileResponse, loggingObj);
										}
									},null,documentId
								);
							} else {
								loggingObj.responseMessage = 'Failed to generate document.';
								sendResponse(res, documentResponse, loggingObj);
							}
						});
					});


			} else {
				getWordDocumentTags(templatePath, function (success, documentTags) {
					if (success) {

						var myDataExtractor = new dataExtractor({
							contactId: contactId,
							customer: customer,
							servicesUrl: services,
							token: token,
							username: username,
							debugMode: debugMode
						});

						myDataExtractor.getDocumentData(entity, entityId, documentTags).then(function (documentData) {

							if (debugMode) {
								console.log(callId + ' ' + customer + ' - Document ' + documentId + ' - ' + entity + ' ' + entityId + ' - Successfully got document data');
							}

							var generateDocumentFunction = generateWordDocument;
							if (fileType) {
								if (fileType.toLowerCase() === 'Excel') {
									generateDocumentFunction = generateExcelDocument;
								}
							}

							generateDocumentFunction(templatePath, documentData, function (success, documentResponse) {
								loggingObj.fileType = documentResponse.fileType;
								if (success) {
									console.log('tags,documentTemplate.DocumentTemplate_ID',documentTemplate.DocumentTemplate_ID)
									//Save it
									var fileName = String(documentTemplate.Name).replace(/\W+/g, '');
									saveFile(
										documentResponse,
										fileName,
										saveLocation,
										customer,
										entity,
										entityId,
										token,
										function (success, saveFileResponse) {
											if (success) {
												loggingObj.url = saveFileResponse.Url;
												loggingObj.success = true;
												loggingObj.responseMessage = 'Success';
												//To help with debugging
												documentData.SaveFileResponse = saveFileResponse;
												documentData.DocumentTags = documentTags;
												sendResponse(res, documentData, loggingObj);
											} else {
												loggingObj.responseMessage = 'Failed to save document.';
												sendResponse(res, saveFileResponse, loggingObj);
											}
										},null, documentResponse.DocumentTemplate_ID
									);
								} else {
									loggingObj.responseMessage = 'Failed to generate document.';
									sendResponse(res, documentResponse, loggingObj);
								}
							});

						}).catch(function (error) {
							console.log('Error getting document data');
							console.log(error);
							loggingObj.responseMessage = error.data;
							sendResponse(res, error, loggingObj);
						});
					} else {
						loggingObj.responseMessage = 'Failed to find a Word Template when getting Document Tags.';
						sendResponse(res, documentTemplate, loggingObj);
					}
				});
			}
		} else {
			loggingObj.responseMessage = 'Failed to find a Word Template.';
			sendResponse(res, documentTemplate, loggingObj);
		}
	});
});

router.route('/:Customer/:Entity/:Entity_ID/Document/:Document_ID').post(function (req, res) {
	var callId = createGUID();
	var customer = req.params.Customer;
	var entity = req.params.Entity;
	var entityId = req.params.Entity_ID;
	var documentId = req.params.Document_ID;
	var fileType = req.query.FileType;
	var token = req.query.token;
	var contactId = req.query.Contact_ID;
	var documentData = req.body;
	var saveLocation = mediaDirectory + '/' + customer + '/' + entity + '/' + entityId;

	var loggingObj = {
		contactId: contactId ? Number(contactId) : null,
		documentData: documentData,
		documentId: documentId,
		entity: entity,
		entityId: entityId,
		guid: callId,
		tenantName: customer,
		success: false
	};

	if (debugMode) {
		console.log(callId + ' ' + customer + ' - Document ' + documentId + ' - ' + entity + ' - Post Create Document');
	}


	//Get document template record from the database
	getDocumentTemplate(customer, documentId, function (success, documentTemplate) {
		if (success) {
			loggingObj.name = documentTemplate.Name;

			var templatePath = mediaDirectory + '/' + documentTemplate.URL.replaceAll('/', '\\');

			//Decide whether or not to make an excel document
			var generateDocumentFunction = generateWordDocument;
			if (fileType) {
				if (fileType.toLowerCase() === 'Excel') {
					generateDocumentFunction = generateExcelDocument;
				}
			}

			generateDocumentFunction(templatePath, documentData, function (success, documentResponse) {
				console.log('getdoctemplate2',documentResponse.DocumentTemplate_ID)
				loggingObj.fileType = documentResponse.fileType;
				if (success) {

					//Save it
					var fileName = String(documentTemplate.Name).replace(/\W+/g, '');
					saveFile(
						documentResponse,
						fileName,
						saveLocation,
						customer,
						entity,
						entityId,
						token,
						function (success, saveFileResponse) {
							if (success) {
								loggingObj.url = saveFileResponse.Url;
								loggingObj.success = true;
								loggingObj.responseMessage = 'Success';
								//To help with debugging
								documentData.SaveFileResponse = saveFileResponse;

								sendResponse(res, documentData, loggingObj);
							} else {
								loggingObj.responseMessage = 'Failed to save document.';
								sendResponse(res, saveFileResponse, loggingObj);
							}
						},null, documentResponse.DocumentTemplate_ID
					);
				} else {
					loggingObj.responseMessage = 'Failed to generate document.';
					sendResponse(res, documentResponse, loggingObj);
				}
			});
		} else {
			loggingObj.responseMessage = 'Failed to find a Word Template.';
			sendResponse(res, documentTemplate, loggingObj);
		}
	});
});

/**
 * @deprecated pass fileType query param to /:Customer/:Entity/:Entity_ID/Document/:Document_ID
 *             currently accepts excel and defaults to word
 */
router.route('/:Customer/:Entity/:Entity_ID/Document/:Document_ID/Excel').get(function (req, res) {
	//To help differentiate calls that may start while others are going
	var callId = createGUID();

	var customer = req.params.Customer;
	var entity = req.params.Entity;
	var entityId = req.params.Entity_ID;
	var documentId = req.params.Document_ID;
	var saveLocation = mediaDirectory + '/' + customer + '/' + entity + '/' + entityId;

	var loggingObj = {
		documentId: documentId,
		entity: entity,
		entityId: entityId,
		guid: callId,
		tenantName: customer,
		success: false
	};

	if (debugMode) {
		console.log(callId + ' ' + customer + ' - Document ' + documentId + ' - ' + entity + ' ' + entityId + ' - Create Excel Document');
	}

	//TODO maybe use token from the header params?
	//Get token so we can make authorized calls
	login(function (success, loginResponse) {
		if (success) {
			var token = loginResponse.token;
			if (debugMode) {
				console.log(callId + ' ' + customer + ' - Document ' + documentId + ' - ' + entity + ' ' + entityId + ' - Token ' + token);
			}

			//Get document template record from the database
			getDocumentTemplate(customer, documentId, function (success, documentTemplate) {
				if (success) {
					loggingObj.name = documentTemplate.Name;

					//Get municipality information
					dataCollector.getCustomerInfoData(customer, function (success, customerInfoRecord) {
						if (success) {

							//Get entity data
							dataCollector.getEntityData(entity, entityId, token, customer, function (success, responseData) {
								if (success) {

									//Build up object for document to parse
									var dataObj = {
										Municipality: customerInfoRecord
									};

									//flatten response for the Word documents
									for (var i in entityData) {
										dataObj[i] = entityData[i];
									}

									//Make the word document
									var templatePath = mediaDirectory + '/' + documentTemplate.URL.replaceAll('/', '\\');
									generateExcelDocument(templatePath, dataObj, function (success, documentResponse) {

										loggingObj.fileType = documentResponse.fileType;
										if (success) {

											//Save it
											var fileName = String(documentTemplate.Name).replace(/\W+/g, '');
											saveFile(
												documentResponse,
												fileName,
												saveLocation,
												customer,
												entity,
												entityId,
												token,
												function (success, saveFileResponse) {
													if (success) {
														loggingObj.url = saveFileResponse.Url;
														loggingObj.success = true;
														loggingObj.responseMessage = 'Saved ' + fileName + ' to ' + saveLocation;
														sendResponse(res, saveFileResponse, loggingObj);
													} else {
														loggingObj.responseMessage = 'Failed to save document.';
														sendResponse(res, saveFileResponse, loggingObj);
													}
												},null,documentResponse.DocumentTemplate_ID
											);
										} else {
											loggingObj.responseMessage = 'Failed to generate document.';
											sendResponse(res, documentResponse, loggingObj);
										}
									});
								} else {
									loggingObj.responseMessage = 'Failed to retrieve data.';
									sendResponse(res, responseData, loggingObj);
								}
							});
						} else {
							loggingObj.responseMessage = 'Failed to load CustomerInfo.';
							sendResponse(res, customerInfoRecord, loggingObj);
						}
					});
				} else {
					loggingObj.responseMessage = 'Failed to find an Excel Template.';
					sendResponse(res, documentTemplate, loggingObj);
				}
			});
		} else {
			loggingObj.responseMessage = 'Failed to login.';
			sendResponse(res, loginResponse, loggingObj);
		}
	});
});

router.route('/:Customer/:Entity/:Entity_ID/Pass/:Pass_ID/Document/:Document_ID').get(function (req, res) {
	// :Entity = 'SQLReport', :Entity_ID = SQLReport_ID
	var data = {
		customer: req.params.Customer,
		document_ID: req.params.Document_ID,
		entity: req.params.Entity,
		entity_ID: req.params.Entity_ID,
		mailmerge: false,
		pass_ID: req.params.Pass_ID,
		excel: false,
		origReq: req,
		origRes: res,
		callback: generateParkingPassDocument
	};

	loadCustomerInfoRecord(data);
});

router.route('/Cashiering/:SQLReport').get(function (req, res) {
	var sqlReport = req.params.SQLReport;
	var startDate = req.query.startdate;
	var endDate = req.query.enddate;
	var stationID = req.query.stationID;

	sqlReport_ID = {
		"DailyRecap": '191', //done
		"TenderDetail": "193", //done
		"BankAccountDistributionDetail": "194", //done
		"RemittanceSummary": "192", //done
		"TransactionListings": "195", //done
		"TenderSummary": "196" //done
	};
	// :Entity = 'SQLReport', :Entity_ID = SQLReport_ID
	var file = 'D:\\Municity Media\\Cashiering\\Yonkers\\' + sqlReport + '\\' + sqlReport + '.docx';

	fs.readFile(file, function (err, fileData) {
		if (err) throw err;
		var zip = new JSZip(fileData);
		var doc = new Docxtemplater().loadZip(zip).setOptions({
			parser: angularParser
		});

		login(function (success, loginData) {
			if (success) {
				var token = loginData.token;
				var getDataUrl = services + '/Customer/Cashiering/SQLReport/' + sqlReport_ID[sqlReport];

				var SearchFilter = {
					"StartDate": startDate,
					"EndDate": endDate,
					"StationId": stationID
				};

				request({
					method: 'POST',
					url: getDataUrl,
					json: SearchFilter,
					headers: {
						'Authorization': 'Token ' + token
					},
					gzip: true,
				}, function (error, response, body) {
					if (!error && response.statusCode == 200) {
						var collectedData = body;

						isObject = function (a) {
							return (!!a) && (a.constructor === Object);
						};

						if (isObject(collectedData)) {

							var tempData = {};
							var count = 0;
							for (var i in collectedData) {
								tempData['Query' + count] = collectedData[i];
								count++;
							}
							collectedData = tempData;
							collectedData.startDate = startDate;
							collectedData.endDate = endDate;
						} else {
							collectedData = {
								startDate: startDate,
								endDate: endDate,
								pages: collectedData
							};
						}

						doc.setData(collectedData);
						try {
							doc.render();

						} catch (error) {
							var e = {
								message: error.message,
								name: error.name,
								stack: error.stack,
								properties: error.properties,
							}
							/*console.log(JSON.stringify({
							    error: e
							}));*/
							throw error;
						}
					} else {
						/*console.log(error);*/
					}
					var buf = doc.getZip()
						.generate({
							type: 'nodebuffer'
						});
					//fs.writeFileSync(path.resolve(__dirname, 'single/' + i + 'Vancant Building Invoice.docx'), buf);
					var date = moment().format('MM-DD-YY_hhmmss');
					mkdirp('D:\\Municity Media\\Cashiering\\Yonkers\\' + sqlReport, function (err) {
						if (err) console.error(err)
						else {
							try {
								fs.writeFile('D:\\Municity Media\\Cashiering\\Yonkers\\' + sqlReport + '\\' + sqlReport + date + '.docx', buf, null, function () {
									/* console.log('success writing file');*/
									res.send({
										Success: true,
										Response: 'https://www.municitymedia.com/Cashiering/Yonkers/' + sqlReport + '/' + sqlReport + date + '.docx'
									});
									//writeToDocumentsTable(customer, entity, entity_ID, documentName + date + extension, entity + '/' + entity_ID, callback, res, token);
								});
							} catch (error) {}
						}
					});
				});
			} else {

			}
		});
	});
});

router.route('/Cashiering/:SQLReport/Excel').get(function (req, res) {
	var sqlReport = req.params.SQLReport;
	var startDate = req.query.startdate;
	var endDate = req.query.enddate;
	var stationID = req.query.stationID;

	sqlReport_ID = {
		"DailyRecap": '191', //done
		"TenderDetail": "193", //done
		"BankAccountDistributionDetail": "194", //done
		"RemittanceSummary": "192", //done
		"TransactionListings": "195", //done
		"TenderSummary": "196" //done
	};
	// :Entity = 'SQLReport', :Entity_ID = SQLReport_ID
	var file = 'D:\\Municity Media\\Cashiering\\Yonkers\\' + sqlReport + '\\' + sqlReport + 'Excel.xlsx';

	fs.readFile(file, function (err, fileData) {
		if (err) throw err;
		var excelTemplate = new XlsxTemplate(fileData);

		login(function (success, loginData) {
			if (success) {
				var token = loginData.token;
				var getDataUrl = services + '/Customer/Cashiering/SQLReport/' + sqlReport_ID[sqlReport];

				var SearchFilter = {
					"StartDate": startDate,
					"EndDate": endDate,
					"StationId": stationID
				};
				console.log(SearchFilter);
				console.log(getDataUrl);
				console.log(token);

				request({
					method: 'POST',
					url: getDataUrl,
					json: SearchFilter,
					headers: {
						'Authorization': 'Token ' + token
					},
					gzip: true,
				}, function (error, response, body) {
					if (!error && response.statusCode == 200) {
						var collectedData = body;
						var tempData = {};
						var count = 0;
						for (var i in collectedData) {
							tempData['Query' + count] = collectedData[i];
							count++;
						}

						collectedData = tempData;
						//collectedData.startDate = String(startDate);
						//collectedData.endDate = String(endDate);
						//console.log('collectedData', collectedData.Query1);
						excelTemplate.substitute(1, collectedData);
						var buf = excelTemplate.generate();
						extension = '.xlsx';
						//fs.writeFileSync(path.resolve(__dirname, 'single/' + i + 'Vancant Building Invoice.docx'), buf);
						var date = moment().format('MM-DD-YY_hhmmss');
						mkdirp('D:\\Municity Media\\Cashiering\\Yonkers\\' + sqlReport, function (err) {
							if (err) console.error(err)
							else {
								try {
									fs.writeFile('D:\\Municity Media\\Cashiering\\Yonkers\\' + sqlReport + '\\' + sqlReport + date + 'Excel.xlsx', buf, 'binary', function () {
										console.log('success writing file');
										res.send({
											Success: true,
											Response: 'https://www.municitymedia.com/Cashiering/Yonkers/' + sqlReport + '/' + sqlReport + date + 'Excel.xlsx'
										});
										//writeToDocumentsTable(customer, entity, entity_ID, documentName + date + extension, entity + '/' + entity_ID, callback, res, token);
									});
								} catch (error) {}
							}
						});
					} else {
						console.log(error);
					}

				});
			}
		});
	});

});


router.route(':Customer/ParcelCard/:PrintKey').get(function (req, res) {
	var data = {
		customer: req.params.Customer,
		document_ID: '11164',
		entity: 'SQLReport',
		entity_ID: '198',
		mailmerge: false,
		printKey: req.params.PrintKey,
		excel: false,
		origReq: req,
		origRes: res,
		callback: generateParcelCard
	};

	console.log('Parcel Card', data.printKey);
	loadCustomerInfoRecord(data);
});

router.route('/:Customer/CollatedQuery/:id/Print').post(function (req, res) {
	console.log('hit collated query', req.body);
	var options = {
		customer: req.params.Customer,
		id: req.params.id,
		origReq: req,
		origRes: res,
		body: req.body,
		fileUrl: req.body.fileUrl, // has to be removed
		entitySavedAs: req.body.entitySavedAs, // has to be removed
		entitySaveAsID: req.body.entitySaveAsID, // has to be removed
		bulkDocumentSaveAsID: req.body.bulkDocumentSaveAsID, // has to be removed
		bulkDocumentSaveAs: req.body.bulkDocumentSaveAs, // has to be removed
		documentName: req.body.documentName,
		preview: req.body.preview,
		filterRecords: req.body.filterRecords,
		GlobalParams: req.body.GlobalParams
	};


	var loggingObj = {
		returnedData: {},
		success: false,
		responseMessage: 'Failed'
	};

	login(function (success, loginData) {
		if (success) {
			options.token = loginData.token;
			GetCollatedData(options, function (returnedData, response, error) {
				//console.log('error', error);
				if (!error) {
					loggingObj.returnedData = returnedData;
					loggingObj.success = true;
					loggingObj.responseMessage = 'Success';
					//console.log('options', options);

					options.collatedData = returnedData.GetCollatedData;



					generateBulkDocument(options);
					//res.send(returnedData.GetCollatedData);
				} else {
					// console.log('collated query failed');
					//	loggingObj.responseMessage = 'Collated Query Failed';
					res.send(loggingObj);
				}
			});
		} else {
			loggingObj.responseMessage = 'Failed to Login';
			sendResponse(options.origRes, {}, loggingObj);
		}
	});
});

function generateBulkDocument(options) {
	//console.log('options', options.collatedData);
	/*options.fileUrl = {}; // Document template object, we need the url .
	options.origReq = {}; // i need the original request.
	options.origRes = {}; // I need the orignal response to reply to. 
	options.collatedData = {}; // The Collated Data( needs to be an array of available data that will be used for each template)
	options.entitySavedAs = ''; // Set as the entity the single documents need to be saved on. I.E Fee and Fee_ID or 'Occupant' and Occupant_ID
	options.entitySaveAsID = ''; // field for entity_ID for individual records
	options.bulkDocumentSaveAs = ''; //Entity for bulk document to be saved to .
	options.bulkDocumentSaveAsID = ''; //Field of id of the place to put the bulk document. 
	options.customer = ''; //which customer are we working on? Tenant.TenantName.
	options.documentName = ''; // I need to figure out where this goes. 
	options.token = ''; //login token*/

	var file = directoryDriveLetter + '\\Municity Media\\' + options.fileUrl.replaceAll('/', '\\'),
		collatedData = options.collatedData;

	// if global parameters = true, do not parse data
	if (!options.GlobalParams) {
		collatedData = JSON.parse(collatedData);
	}

	//console.log('file: '+ file);

	fs.readFile(file, function (err, templateFile) {
		if (err) {
			options.origRes.send({
				Success: false + ' No file found',
				Response: err
			});
			//Having this here causes it to crash
			//throw err;
		} else {
			var zip = new JSZip(templateFile);
			var doc = new Docxtemplater().loadZip(zip).setOptions({
				parser: angularParser
			});
			doc.loadZip(zip);
			//options.filterRecords = [38709];
			//console.log('options.filterRecords', options.filterRecords);
			if (options.filterRecords) {
				//console.log('length', collatedData.length);
				for (var m = 0; m < collatedData.length; m++) {
					//console.log('data', m, collatedData[m]['Occupant_ID'], options.filterRecords.indexOf(collatedData[m]['Occupant_ID']));
					console.log('options.filterRecords', options.filterRecords, collatedData[m]['Occupant_ID'], options.filterRecords.includes(Number(collatedData[m]['Occupant_ID'])));
					if (options.filterRecords.includes(Number(collatedData[m]['Occupant_ID']))) {
						console.log('found');
						//console.log('skip');
					} else {
						console.log('cutting', 'm', m, 'cd', collatedData[m].Occupant_ID);
						//collatedData.splice(m, 1);
						collatedData = collatedData.filter(val => options.filterRecords.includes(val.Occupant_ID));
					}
				}
			}
			console.log('collatedData', collatedData);

			for (var i = 0; i < collatedData.length; i++) {

				var singleZip = new JSZip(templateFile);
				var singleDoc = new Docxtemplater().loadZip(singleZip).setOptions({
					parser: angularParser
				});

				singleDoc.loadZip(singleZip);
				console.log('collatedData[i]', collatedData[i]);
				//Collection Data needs to be the data from the collatedQuery Call ( must be passed in as an option)
				singleDoc.setData(collatedData[i]);
				//var feeID = collectedData[i][type + '_ID'];
				var entity_ID = collatedData[i][options.entitySaveAsID]; //Example this is Data[specific record][Fee_ID]
				console.log('entity_ID', entity_ID);
				try {
					singleDoc.render(); //Does the template work?

				} catch (error) { // it doesnt.
					var e = {
						message: error.message,
						name: error.name,
						stack: error.stack,
						properties: error.properties,
					}
					options.origRes.send({
						Success: false,
						Response: e
					});

					throw error;
				}

				var singleFileSaveLocation = directoryDriveLetter + '\\Municity Media\\' + options.customer + '\\'
				if (options.preview) {
					singleFileSaveLocation += 'Preview' + '\\';
				}
				singleFileSaveLocation += options.entitySavedAs + '\\' + entity_ID + '\\';

				//function saveFile(doc, fileName, saveLocation, customer, entity, entityId, token, callback) {
				saveFile(singleDoc, options.documentName, singleFileSaveLocation, options.customer, options.entitySavedAs, entity_ID, options.token, function (success, writeFileResponse) {

				}, options.preview);
			}

			Object.keys(doc.zip.files).forEach(function (f) {
				var asTextOrig = doc.zip.files[f].asText;
				doc.zip.files[f].asText = function () {
					var text = asTextOrig.apply(doc.zip.files[f]);
					text = text.replace("<w:body>", "<w:body><w:t>{#pages}</w:t>");
					text = text.replace("</w:body>", '<w:br w:type="page" /><w:t>{/pages}</w:t></w:body>');
					return text;
				};
			});


			collatedData = {
				pages: collatedData,
				PrintedBy: '' //,
				//Customerinfo: data.customerInfoRecord
			};

			doc.setData(collatedData);
			try {
				doc.render();

			} catch (error) {
				var e = {
					message: error.message,
					name: error.name,
					stack: error.stack,
					properties: error.properties,
				}
				data.origRes.status(404).send({
					success: 'false',
					error: e
				});
			}
			var saveMergedLocation = directoryDriveLetter + '\\Municity Media\\' + options.customer + '\\'
			if (options.preview) {
				saveMergedLocation += 'Preview' + '\\';
			}
			saveMergedLocation += options.bulkDocumentSaveAs + '\\' + options.bulkDocumentSaveAsID + '\\';

			//function saveFile(doc, fileName, saveLocation, customer, entity, entityId, token, callback) {
			//saveFile(singleDoc, options.documentName, singleFileSaveLocation, options.customer, options.entitySavedAs, entity_ID, options.token, function (success, writeFileResponse) {

			saveFile(doc, options.documentName, saveMergedLocation, options.customer, options.bulkDocumentSaveAs, options.bulkDocumentSaveAsID, options.token, function (success, writeFileResponse) {
				console.log('done');
				// .send gets called twice if the doc fails to render (the data.origRes.send above this);
				options.origRes.send({
					Success: true,
					Response: writeFileResponse
				});

			}, options.preview);

		}
	});

	/*
	/**
	 * @desc Save the file to the server and create an entry in the Documents table.
	 * @param {object} doc The document object to be written to a file.
	 * @param {string} fileName What to name the file.
	 * @param {string} saveLocation Where to save the file.
	 * @param {customer} customer TenantName of who the file belongs to.
	 * @param {entity} entity Entity type the file belongs to.
	 * @param {int} entityId The _ID of the entity the file belongs to.
	 * @param {string} token Token to authorize the save
	 * @param {function} callback Callback function, returns success and a response
	 *                            object
	 
	function saveFile(doc, fileName, saveLocation, customer, entity, entityId, token, callback) {
	*/


}

function GetCollatedData(data, callback) {
	var getDataUrl = services + '/Customer/' + data.customer + '/CollatedQuery/' + data.id,
		billing_id = data.body.bulkDocumentSaveAsID,
		params = null;

	if (data.body.GlobalParams) {
		params = {
			"GlobalParams": {
				"@Billing_ID": billing_id
			}
		}
	}
	//console.log('getdataurl', getDataUrl);
	//console.log('data', data.body, data.token);
	delete data.body.fileUrl;
	delete data.body.entitySavedAs;
	delete data.body.entitySaveAsID;
	delete data.body.bulkDocumentSaveAs;
	delete data.body.bulkDocumentSaveAsID;
	delete data.body.documentName;
	delete data.body.GlobalParams;

	request({
		method: 'POST',
		url: getDataUrl,
		json: params,
		headers: {
			'Authorization': 'Token ' + data.token
		},
		gzip: true,
	}, function (error, response, body) {
		if (!error && response.statusCode == 200) {
			//console.log('body', body);
			data.GetCollatedData = body;
			callback(data, response);
		} else {
			//	console.log('failed get data');
			callback(data, response, error);
		}
	});
}

/**
 * @desc Create a unique-ish ID
 * @return {string} a unique-ish ID
 */
function createGUID() {
	var callId = String(Math.round(Math.random() * 10000));
	var pad = "00000"; //#leftpad
	return new Date().toISOString() + '_' + pad.substring(0, pad.length - callId.length) + callId;
}

/**
 * @desc Login Function to get token and accessible tenants
 *
 * @param {function} callback Callback function, returns success, data, error,
 *                            and response
 */
//TODO? support user and password params
function login(callback) {
	var m5Services = services;
	m5Services = m5Services.split('/');
	m5Services.pop();

	var loginUrl = m5Services.join('/');
	loginUrl += '/Login/Login.svc';
	loginUrl += '/PostM5Login';

	request({
		method: 'POST',
		json: {
			user: 'superadmin',
			pwd: 'm54lyfe'
		},
		url: loginUrl,
		gzip: true
	}, function (error, response, data) {
		if (!error && response.statusCode == 200) {
			callback(true, data, error, response);
		} else {
			if (debugMode) {
				callback(false, data || error || response);
			} else {
				callback(false);
			}
		}
	});
}

/**
 * @deprecated Use dataCollector.getCustomerInfoData
 */
function loadCustomerInfoRecord(data) {
	login(function (success, loginResponse) {
		if (success) {
			var token = loginResponse.token;
			//	console.log('token', token);
			request({
				method: 'GET',
				json: true,
				url: services + '/Customer/' + data.customer + '/DocumentTemplate',
				gzip: true,
			}, function (error, response, body) {
				//	console.log('==============================================');
				//	console.log(data.document_ID);
				if (!error && response.statusCode == 200) {
					var templates = body.TenantTemplates;
					var documentURL;
					for (var i in templates) {
						if (templates[i].DocumentTemplate_ID == data.document_ID) {
							//	console.log('We have a matching documentid ==================');
							documentObject = templates[i];
							request({
								method: 'GET',
								json: true,
								url: services + '/Customer/' + data.customer + '/CustomerInfo',
								gzip: true,
							}, function (error, response, body) {
								if (!error && response.statusCode == 200) {
									var access = false;
									//	console.log('Access? ========================');
									var tenant_ID
									for (var i in loginResponse.serviceaccess) {
										if (loginResponse.serviceaccess[i].TenantName == data.customer) {
											//	console.log(loginResponse.serviceaccess[i].TenantName);
											//		console.log('found customer record', loginResponse.serviceaccess[i]);
											data.tenant_ID = loginResponse.serviceaccess[i].Tenant_ID;
											if (!loginResponse.serviceaccess[i].UseTenant_ID) {
												data.tenant_ID = null;
											}
											access = true;
											data.customerInfoRecord = body;
											data.documentObject = documentObject;
											data.token = token;
											if (data.callback) {
												data.callback(data);
											}
										}

									}
									if (!access) {
										data.origRes.send({
											Success: 'error',
											Response: 'No Access to Superadmin.'
										});
									}
								} else {
									data.origRes.send({
										Success: 'error',
										Response: 'Failure to login.'
									});
								}
							});
						}
					}
				}
			});
		} else {
			data.origRes.send({
				Success: error,
				Response: 'Failure to login.'
			});
		}
	});
}

/**
 * @deprecated use generateWordDocument or generateExcelDocument instead
 */
//customer, document_ID, entity, entity_ID, mailmerge, req, res, customerInfoRecord, documentObject, token, startDate, endDate, tenant_ID, excel
function generateDocument(data) {

	var file = directoryDriveLetter + '\\Municity Media\\' + data.documentObject.URL.replaceAll('/', '\\');
	fs.readFile(file, function (err, fileData) {
		if (err) {
			data.origRes.send({
				Success: false + ' No file found',
				Response: err
			});
			//Having this here causes it to crash
			//throw err;
		} else {
			if (data.excel) {
				var excelTemplate = new XlsxTemplate(fileData);

			} else {
				var zip = new JSZip(fileData);
				var doc = new Docxtemplater().loadZip(zip).setOptions({
					parser: angularParser
				});
			}
			dataCollector.getData(data, function (collectedData, type) {
				//callback
				if (!collectedData) {
					data.origRes.status(404).send({
						Success: 'No Data Found from Filtered Search Call',
						Response: 'error'
					});
				}
				if (!data.entity == 'SQLReport') {
					collectedData = body.Data;
				}
				if (data.entity === 'Billing') {
					collectedData = collectedData.Data;
				}

				//console.log('collectedData', collectedData);
				if (!data.excel) {
					doc.loadZip(zip);
				}
				if (data.mailmerge) {
					for (var i = 0; i < collectedData.length; i++) {
						var singleZip = new JSZip(fileData);
						var singleDoc = new Docxtemplater().loadZip(singleZip).setOptions({
							parser: angularParser
						});

						singleDoc.loadZip(singleZip);
						singleDoc.setData(collectedData[i]);
						var feeID = collectedData[i][type + '_ID'];
						try {
							singleDoc.render();

						} catch (error) {
							var e = {
								message: error.message,
								name: error.name,
								stack: error.stack,
								properties: error.properties,
							}
							data.origRes.send({
								Success: false,
								Response: e
							});

							throw error;
						}

						var saveLocation = directoryDriveLetter + '\\Municity Media\\' + data.customer + '\\' + type + '\\' + feeID + '\\';
						saveDoc(data.customer, singleDoc, saveLocation, type, feeID, data.documentName, function (success, writeFileResponse) {
							data.origRes.send({
								Success: true,
								Response: writeFileResponse
							});
						}, data.origRes, data.token);
					}
					Object.keys(doc.zip.files).forEach(function (f) {

						var asTextOrig = doc.zip.files[f].asText;

						doc.zip.files[f].asText = function () {
							var text = asTextOrig.apply(doc.zip.files[f]);
							text = text.replace("<w:body>", "<w:body><w:t>{#pages}</w:t>");
							text = text.replace("</w:body>", '<w:p w:rsidR="00C7053A" w:rsidRDefault="00C7053A"><w:r><w:rPr><w:lang w:val="fr-FR" /></w:rPr><w:br w:type="page" /></w:r></w:p><w:t>{/pages}</w:t></w:body>');
							return text;
						};

					});

					collectedData = {
						pages: collectedData,
						PrintedBy: '',
						Customerinfo: data.customerInfoRecord

					};
				}

				isObject = function (a) {
					return (!!a) && (a.constructor === Object);
				};

				if (isObject(collectedData) && data.entity != 'Billing') {

					var tempData = {};
					var count = 0;
					for (var i in collectedData) {
						tempData['Query' + count] = collectedData[i];
						count++;
					}
					collectedData = tempData;
				} else if (data.entity == 'SQLReport') {
					collectedData = {
						pages: collectedData,
					};
				}
				collectedData.startDate = data.startDate;
				collectedData.endDate = data.endDate;
				collectedData.customerInfoRecord = data.customerInfoRecord;

				if (data.excel) {
					try {
						excelTemplate.substitute(1, collectedData);

					} catch (error) {

					}
					try {
						excelTemplate.substitute(2, collectedData);

					} catch (error) {

					}
					try {
						excelTemplate.substitute(3, collectedData);

					} catch (error) {

					}
					doc = excelTemplate;
				} else {
					doc.setData(collectedData);
					try {
						doc.render();

					} catch (error) {
						var e = {
							message: error.message,
							name: error.name,
							stack: error.stack,
							properties: error.properties,
						}
						data.origRes.status(404).send({
							success: 'false',
							error: e
						});
					}
				}
				var saveMergedLocation = directoryDriveLetter + '\\Municity Media\\' + data.customer + '\\' + data.entity + '\\' + data.entity_ID + '\\';
				saveDoc(data.customer, doc, saveMergedLocation, data.entity, data.entity_ID, data.documentName, function (success, writeFileResponse) {

					// .send gets called twice if the doc fails to render (the data.origRes.send above this);
					data.origRes.send({
						Success: true,
						Response: writeFileResponse
					});

				}, data.origRes, data.token, data.excel);
			});

		}
	});
}

/**
 * @desc Get a document template record.
 * @param {string} customer Which customer the document belongs to.
 * @param {int} documentId The ID of the document you want.
 * @param {function} callback Callback function, returns success and a response
 *                            object.
 */
function getDocumentTemplate(customer, documentId, callback) {
	var documentTemplateUrl = services;
	documentTemplateUrl += '/Customer/' + customer;
	documentTemplateUrl += '/DocumentTemplate';
	//console.log(documentTemplateUrl);

	request({
		method: 'GET',
		json: true,
		url: documentTemplateUrl,
		gzip: true,
	}, function (error, response, body) {
		if (!error && response.statusCode === 200) {
			//Need to loop through because doing /DocumentTemplate/Document_ID returns
			//the actual file
			var templates = body.TenantTemplates;
			var template = null;
			for (var i in templates) {
				if (templates[i].DocumentTemplate_ID == documentId) {
					template = templates[i];
				}
			}

			if (template) {
				callback(true, template);
			} else {
				console.log('Couldn\'t find a matching DocumentTemplate');
				console.log('Document_ID', documentId);
				console.log(documentTemplateUrl);
				if (debugMode) {
					callback(false, {
						message: 'Couldn\'t find a matching DocumentTemplate',
						templates: body
					});
				} else {
					callback(false);
				}
			}
		} else {
			console.log('Failed to get DocumentTemplates');
			console.log(documentTemplateUrl);
			console.log('status code: ' + response.statusCode);
			console.log(error);
			if (debugMode) {
				callback(false, body);
			} else {
				callback(false);
			}
		}
	});
}

/**
 * @desc Create the tags in a Word Document.
 * @param {string} templatePath The path to the template file.
 * @param {function} callback Callback function, returns success and a response
 *                            object.
 */
function getWordDocumentTags(templatePath, callback) {
	fs.readFile(templatePath, function (err, fileData) {
		if (!err) {
			var zip = new JSZip(fileData);
			var doc = new Docxtemplater().loadZip(zip).setOptions({
				parser: angularParser
			});

			var InspectModule = require("docxtemplater/js/inspect-module");
			var iModule = InspectModule();
			doc.attachModule(iModule);

			try {
				doc.render();
				var documentTags = iModule.getAllTags();
				callback(true, documentTags);
			} catch (error) {
				console.log('Failed to get document tags');
				console.log(error);
				if (debugMode) {
					callback(false, error);
				} else {
					callback(false);
				}
			}
		} else {
			console.log('Failed to read Word document template');
			console.log('templatePath: ' + templatePath);
			console.log(err);
			if (debugMode) {
				callback(false, err);
			} else {
				callback(false);
			}
		}
	});
}

function base64DataURLToArrayBuffer(dataURL) {
	const base64Regex = /^data:image\/(png|jpg|svg|svg\+xml);base64,/;
	if (!base64Regex.test(dataURL)) {
		return false;
	}
	const stringBase64 = dataURL.replace(base64Regex, "");
	let binaryString;
	if (typeof window !== "undefined") {
		binaryString = window.atob(stringBase64);
	} else {
		binaryString = Buffer.from(stringBase64, "base64").toString("binary");
	}
	const len = binaryString.length;
	const bytes = new Uint8Array(len);
	for (let i = 0; i < len; i++) {
		const ascii = binaryString.charCodeAt(i);
		bytes[i] = ascii;
	}
	return bytes.buffer;
}


/**
 * @desc Create a Word document object.
 * @param {string} templatePath The path to the template file.
 * @param {obj} data The data to pass to the template.
 * @param {function} callback Callback function, returns success and a response
 *                            object.
 */
function generateWordDocument(templatePath, data, callback) {

	fs.readFile(templatePath, function (err, fileData) {
		if (!err) {
			var zip = new JSZip(fileData);
			var opts = {}
			opts.centered = false;

			opts.getSize = function (_, tagValue) {
				if (tagValue.size && tagValue.data) {
					return tagValue.size;
				}
				return [150, 150];
			};

			if (data.QRCODE) {
				getQRCode(opts, data, url => doTheRest(opts))
			} else {
				opts.getImage = function (tagValue) {

					if (tagValue.size && tagValue.data) {
						return fs.readFileSync(tagValue.data, 'binary');
					}
					return fs.readFileSync(tagValue, 'binary');
				};
				doTheRest(opts);
			}




			function getQRCode(opts, data, callback) {

				if (data.QRCODE) {
					QRCode.toDataURL(data.QRCODE, {
						width: '100'
					}, function (err, url) {
						if (err) return;

						opts.getSize = function (_, tagValue) {
							if (tagValue.size && tagValue.data) {
								return tagValue.size;
							}
							return [50, 50];
						};
						opts.getImage = function (tagValue) {
							console.log('qrtag', tagValue);
							if (tagValue == 'QRGENERATE') {
								return (base64DataURLToArrayBuffer(url));
							} else {
								if (tagValue.size && tagValue.data) {
									return fs.readFileSync(tagValue.data, 'binary');
								}
								return fs.readFileSync(tagValue, 'binary');
							}
						}
						callback(opts, url)
					});
				}
			}

			function doTheRest(opts) {
				var imageModule = new ImageModule(opts);

				var doc = new Docxtemplater().loadZip(zip).setOptions({
					parser: angularParser
				}).attachModule(imageModule);

				//testing
				//data.image = [];
				//data.image.push( {'signature':'D:/Municity Media/NorwalkCityCT/Inspection/1237177/employee.png'});
				//data={image:'',test:''}
				doc.setData(data);

				try {
					doc.render();
					callback(true, doc);
				} catch (error) {
					console.log('Failed to render Word template');
					console.log(error);
					if (debugMode) {
						callback(false, error);
					} else {
						callback(false);
					}
				}
			}

			// opts.getImage = 


		} else {
			console.log('Failed to read Word document template');
			console.log('templatePath: ' + templatePath);
			console.log(err);
			if (debugMode) {
				callback(false, err);
			} else {
				callback(false);
			}
		}
	});
}



/**
 * @desc Create an Excel document object.
 * @param {string} templatePath The path to the template file.
 * @param {obj} data The data to pass to the template.
 * @param {function} callback Callback function, returns success and a response
 *                            object.
 */
function generateExcelDocument(templatePath, data, callback) {
	fs.readFile(templatePath, function (err, fileData) {
		if (!err) {
			var doc = new XlsxTemplate(fileData);

			try {
				try {
					doc.substitute(1, data);

				} catch (error) {

				}
				try {
					doc.substitute(2, data);

				} catch (error) {

				}
				try {
					doc.substitute(3, data);

				} catch (error) {

				}
				callback(true, doc);
			} catch (error) {
				console.log('Failed to render Excel template');
				console.log(error);
				if (debugMode) {
					callback(false, error);
				} else {
					callback(false);
				}
			}
		} else {
			console.log('Failed to read Excel document template');
			console.log('filepath: ' + filePath);
			console.log(err);
			if (debugMode) {
				callback(false, err);
			} else {
				callback(false);
			}
		}
	});
}

/**
 * @desc Save the file to the server and create an entry in the Documents table.
 * @param {object} doc The document object to be written to a file.
 * @param {string} fileName What to name the file.
 * @param {string} saveLocation Where to save the file.
 * @param {customer} customer TenantName of who the file belongs to.
 * @param {entity} entity Entity type the file belongs to.
 * @param {int} entityId The _ID of the entity the file belongs to.
 * @param {string} token Token to authorize the save
 * @param {function} callback Callback function, returns success and a response
 *                            object
 */
function saveFile(doc, fileName, saveLocation, customer, entity, entityId, token, callback, preview, DocumentTemplate_ID) {
	console.log('save file', DocumentTemplate_ID);

	var date = moment().format('MM-DD-YY hhmmss');
	var fileType = doc.fileType;

	mkdirp(saveLocation, function (err) {
		if (err) {
			console.log('Failed to make directory for document');
			console.log('saveLocation: ' + saveLocation);
			console.log(err);
			if (debugMode) {
				callback(false, err);
			} else {
				callback(false)
			}
		} else {
			var buf = doc.getZip().generate({
				type: 'nodebuffer'
			});

			var newFileName = fileName
			if (!preview) {
				newFileName += +' ' + date
			}
			newFileName += '.' + fileType;
			var fullPath = saveLocation + '/' + newFileName;
			fs.writeFile(fullPath, buf, 'binary', function (error) {
				if (error) {
					if (debugMode || preview) {
						callback(false, error);
					} else {
						callback(false);
					}
				} else {
					if (!preview) {
						writeToDocumentsTable(customer, entity, entityId, newFileName, saveLocation, token, function (success, response) {
							if (success) {
								callback(true, response);
							} else {
								if (debugMode || preview) {
									callback(false, response);
								} else {
									callback(false, response);
								}
							}
						}, DocumentTemplate_ID);
					} else {
						callback(true, fullPath);
					}
				}
			});
		}
	});
}

//One off thing for Parking Pass
//Parking Pass is only used by LongBeach
/**
 * @desc document function specifically for parking passes, cause they're kinda like sqlReports, but not really
 *
 * @param {object} data - object passed by the Pass/:Pass_ID route. See the route for object structure
 */
function generateParkingPassDocument(data) {

	var file = directoryDriveLetter + '\\Municity Media\\' + documentObject.URL.replaceAll('/', '\\');

	fs.readFile(file, function (err, fileData) {
		if (err) throw err;

		var zip = new JSZip(fileData);
		var doc = new Docxtemplater().loadZip(zip).setOptions({
			parser: angularParser
		});

		var getDataUrl, SearchFilter;
		getDataUrl = services + '/Customer/' + data.customer + '/' + data.entity + '/' + data.entity_ID;
		var extension = data.documentObject.URL.split('.').pop();
		data.documentName = String(data.documentObject.Name).replace(/\W+/g, '');
		SearchFilter = {
			"PassID": data.pass_ID
		};


		request({
			method: 'POST',
			url: getDataUrl,
			json: SearchFilter,
			headers: {
				'Authorization': 'Token ' + data.token
			},
			gzip: true,
		}, function (error, response, body) {
			if (!error && response.statusCode == 200) {
				var collectedData = body;

				isObject = function (a) {
					return (!!a) && (a.constructor === Object);
				};

				if (isObject(collectedData)) {
					//Handle Multi Select queries here
					var tempData = {};
					var count = 0;
					for (var i in collectedData) {
						tempData['Query' + count] = collectedData[i][0];
						count++;
					}
					collectedData = tempData;
				} else {
					collectedData = body[0];
					//Because...newline characters.
					collectedData.Locations = collectedData.Locations.replace('\\n', '\n');
				}

				doc.setData(collectedData);
				try {
					doc.render();

				} catch (error) {
					var e = {
						message: error.message,
						name: error.name,
						stack: error.stack,
						properties: error.properties,
					}
					console.log(JSON.stringify({
						error: e
					}));
					throw error;
				}

				var saveMergedLocation = directoryDriveLetter + '\\Municity Media\\' + data.customer + '\\' + data.entity + '\\' + data.entity_ID + '\\';
				saveDoc(data.customer, doc, saveMergedLocation, data.entity, data.entity_ID, documentName, function (success, writeFileResponse) {
					data.origRes.send({
						Success: true,
						Response: writeFileResponse
					});
				}, data.origRes, data.token, data.excel, null);
			} else {

				res.send({
					Success: false,
					Response: error
				});
			}
		});
	});
}

//One off thing for PAS
//Only used by Scarsdale
/**
 * @desc generate Scarsdale Parcel Card
 *
 * @param {object} data - object passed by the ParcelCard route
 */
function generateParcelCard(data) {
	var file = directoryDriveLetter + '\\Municity Media\\' + documentObject.URL.replaceAll('/', '\\');
	//console.log('Callback', data);

	fs.readFile(file, function (err, fileData) {
		if (err) throw err;

		var zip = new JSZip(fileData);
		var doc = new Docxtemplater().loadZip(zip).setOptions({
			parser: angularParser
		});

		var getDataUrl, SearchFilter;
		getDataUrl = services + '/Customer/' + data.customer + '/' + data.entity + '/' + data.entity_ID;
		var extension = data.documentObject.URL.split('.').pop();
		data.documentName = String(data.documentObject.Name).replace(/\W+/g, '');
		SearchFilter = {
			"PrintKey": data.printKey
		};

		request({
			method: 'POST',
			url: getDataUrl,
			json: SearchFilter,
			headers: {
				'Authorization': 'Token ' + data.token
			},
			gzip: true,
		}, function (error, response, body) {
			if (!error && response.statusCode == 200) {
				var collectedData = body;
				//console.log('Parcel Card', collectedData);

				isObject = function (a) {
					return (!!a) && (a.constructor === Object);
				};

				//I really have no idea what this object structure looks like yet
				doc.setData(collectedData);
				try {
					doc.render();

				} catch (error) {
					var e = {
						message: error.message,
						name: error.name,
						stack: error.stack,
						properties: error.properties,
					}
					console.log(JSON.stringify({
						error: e
					}));
					throw error;
				}

				var saveMergedLocation = directoryDriveLetter + '\\Municity Media\\' + data.customer + '\\' + data.entity + '\\' + data.entity_ID + '\\';
				saveDoc(data.customer, doc, saveMergedLocation, data.entity, data.entity_ID, data.documentName, function (sucess, writeFileResponse) {
					data.origRes.send({
						Success: true,
						Response: writeFileResponse
					});
				}, data.origRes, data.token, data.excel), null;
			} else {

				res.send({
					Success: false,
					Response: error
				});
			}
		});
	});
}

/**
 * @deprecated Use saveFile instead
 */
function saveDoc(customer, doc, saveLocation, entity, entity_ID, documentName, callback, res, token, excel, DocumentTemplate_ID) {

	var extension = '.docx';
	var date = moment().format('MM-DD-YY_hhmmss');
	if (excel) {
		var buf = doc.generate();
		extension = '.xlsx';
		mkdirp(saveLocation, function (err) {
			if (err) console.error(err)
			else {
				try {
					fs.writeFile(saveLocation + documentName + date + extension, buf, 'binary', function () {

						writeToDocumentsTable(customer, entity, entity_ID, documentName + date + extension, entity + '/' + entity_ID, token, callback, DocumentTemplate_ID);
					});
				} catch (error) {}
			}
		});
	} else {
		var buf = doc.getZip()
			.generate({
				type: 'nodebuffer'
			});
		//fs.writeFileSync(path.resolve(__dirname, 'single/' + i + 'Vancant Building Invoice.docx'), buf);
		mkdirp(saveLocation, function (err) {
			if (err) console.error(err)
			else {
				try {
					fs.writeFile(saveLocation + documentName + date + extension, buf, null, function () {

						writeToDocumentsTable(customer, entity, entity_ID, documentName + date + extension, entity + '/' + entity_ID, token, callback, DocumentTemplate_ID);
					});
				} catch (error) {}
			}
		});
	}

}

/**
 * @desc Create an entry in the Documents table.
 * @param {string} customer TenantName of who the file belongs to.
 * @param {entity} entity Entity type the file belongs to.
 * @param {int} entity_ID The _ID of the entity the file belongs to.
 * @param {string} fileName The name of the file including extension.
 * @param {string} directory Where the file is located.
 * @param {string} token Token to authorize the save.
 * @param {function} callback Callback function, returns success and a response
 *                           object
 */
function writeToDocumentsTable(customer, entity, entity_ID, fileName, directory, token, callback, DocumentTemplate_ID) {

	if (1 == 1) {
		var createDocumentRecordUrl = services + '/Customer/' + customer;
		createDocumentRecordUrl += '/' + entity;
		createDocumentRecordUrl += '/' + entity_ID;
		createDocumentRecordUrl += '/Document';
		//console.log(createDocumentRecordUrl);
		var fileData = {
			"Description": fileName + ' uploaded by ',
			"Name": fileName,
			"Type": 'Document',
			"DocumentTemplate_ID": DocumentTemplate_ID
			// "Directory": '' IDK why ? //fuck this attribute
		};
		fileData.Directory = '';
		//console.log(fileData);
		request({
			method: 'POST',
			headers: {
				'Authorization': 'Token '+ token
			  },
			url: createDocumentRecordUrl,
			json: fileData,
			gzip: true,
		}, function (error, response, body) {
			//console.log('response',response,error);

			if (!error && response.statusCode == 200) {
				callback(true, response.body);
			} else {
				if (debugMode) {
					callback(false, error);
				} else {
					callback(false);
				}
			}
		});
	} else {
		callback(true, 'Skipped writing to documents table.');
	}


}

/**
 * @desc Escape characters that could break the xml encoding that the docs use.
 * @param {stirng} unsafe The string to escape.
 * @return {string} Xml safe string.
 */
function escapeXml(unsafe) {
	if (unsafe.replace) {
		return unsafe.replace(/[<>&'"]/g, function (c) {
			switch (c) {
				case '<':
					return '&lt;';
				case '>':
					return '&gt;';
				case '&':
					return '&amp;';
				case '\'':
					return '&apos;';
				case '"':
					return '&quot;';
			}
		});
	} else {
		return unsafe;
	}
}

/**
 * @desc Sends a response to the "res" that is passed in. Also posts to a
 *       logging url.
 * @param {object} res The response object.
 * @param {object} data The debug data to send back. Only sends if debugMode is
 *                      true.
 * @param {object} loggingObj The object to send to the logging url.
 * @param {function} callback Callback function, returns success.
 */
//TODO Maybe clean this up so it isn't relying on loggingObj?
function sendResponse(res, data, loggingObj, callback) {
	var response = {
		ID: loggingObj.guid,
		Success: loggingObj.success,
		Response: loggingObj.responseMessage
	};
	if (loggingObj.success) {
		if (data.SaveFileResponse) {
			response.Response = data.SaveFileResponse;
		} else {
			if (process.env.Production == 'true') {
				response.Response = response.Response + '. But no SaveFileResponse?';
			} else {
				response.Response = response.Response + '. Skipped writing to the documents table, running in Non-Production.';
			}
		}
	}

	//If it is debug mode or there is no responseMessage, send the friendly data
	if (debugMode) {
		var logMessage = '';
		logMessage += loggingObj.guid + ' ' + loggingObj.tenantName;
		logMessage += ' - Document ' + loggingObj.documentId;
		logMessage += ' - ' + loggingObj.entity + ' ' + loggingObj.entityId;
		logMessage += ' - ' + (loggingObj.responseMessage || 'No responseMessage set.');

		console.log(logMessage);

		response.DebugInfo = data || logMessage;
	}

	res.send(response);

	if (loggingObj) {
		logPrintout(loggingObj, callback);
	} else if (callback) {
		callback(true);
	}
}

/**
 * @desc Log the run of the printout.
 * @param {object} options The options to log.
 *                         documentId, entity, entityId, fileType, guid, name,
 *                         tenantid, tenantName, success, url
 * @param {function} callback Callback function.
 */
function logPrintout(options, callback) {

	if (process.env.Production == 'true') {

		//Endpoint expects data in a specific format
		var body = {
			data: {
				attributes: {
					"document-id": options.documentId,
					"entity": options.entity,
					"entity-id": options.entityId,
					"error-message": (options.success ? null : options.responseMessage),
					"file-type": options.fileType,
					"guid": options.guid,
					"name": options.name,
					"tenant-name": options.tenantName,
					"success": options.success,
					"url": options.url
				},
				type: "printouts"
			}
		};

		request.post({
				url: loggerApiUrl + '/printouts',
				headers: {
					"Content-Type": "application/vnd.api+json"
				},
				body: JSON.stringify(body)
			},
			function (error, response, body) {
				if (callback) {
					callback(error, response, body);
				}
			}
		);
	} else if (callback) {
		callback(true);
	}
}

/**
 * @desc Extends the string prototype to add a replaceAll function that will
 *       replace all occurrences of a value in a string.
 * @param {string} search The value to replace.
 * @param {string} replacement What to replace the value with.
 */
String.prototype.replaceAll = function (search, replacement) {
	var target = this;
	return target.replace(new RegExp(search, 'g'), replacement);
};

module.exports = router;