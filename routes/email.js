//========================================================
// Municity Node
// Email Services
//
// Prefix:              /email
// Currently Supports:  /m5password, /custom
//
// Author:          Caleb Wright <cwright@sca-corp.com>
// Created Date:    5/9/2014
// Last Modified:   5/9/2014
//========================================================

const express    = require('express');
const path       = require('path');
const nodemailer = require('nodemailer');
const router     = express.Router();
const pug        = require('pug');
const fs         = require('fs');
const tenant     = require('../util/tenant');

let transportMunicity5 = {};
try { transportMunicity5 = JSON.parse(process.env['TransportMunicity5']); } catch(e) {}

let transportAWS = {};
try { transportAWS = JSON.parse(process.env['TransportAWS']); } catch(e) {}

var GmailTransport = nodemailer.createTransport({
    service: "Gmail",
    secureConnection: true,
    auth: {
        user: transportMunicity5.user,
        pass: transportMunicity5.pass,
    }
});

var Municity5Transport = nodemailer.createTransport({
    host: transportAWS.host, // hostname
    secureConnection: true, // use SSL
    port: transportAWS.port, // port for secure SMTP
    auth: {
        user: transportAWS.user,
        pass: transportAWS.pass,
    }
});

router.post('/m5password', function (req, res) {

    //req.logStream.write(JSON.stringify(req.body)+'\r\n');
    var recipientEmail = req.body.recipientEmail;
    var recipientName  = req.body.recipientName;
    var username       = req.body.username;
    var password       = req.body.password;

    //COMPOSE EMAIL
    var email = {
        from: "The Municity5 Team <Municity5@gmail.com>", // sender address
        to: recipientEmail, // receiver
        subject: 'Municity5 Account Registration', // Subject line
        html: '<p><img src="https://municitymedia.com/M5EmailTemplate/Header.png"/></p>' +
              '<p>' + recipientName + ',<br><br>You have requested that your password be emailed to you.<br><br>' +
              'Sign In At http://Municity5.com/Municity5<br>Your Username: ' + username + '<br>Your Password: ' + password + '<br><br>-The Municity5 Team</p>' +
              '<p><img src="https://municitymedia.com/M5EmailTemplate/Footer.png"/></p>',
        forceEmbeddedImages: true
    };
    //SEND EMAIL FROM MUNICITY 5 GMAIL
    GmailTransport.sendMail(email, function (error, response) {
        if (error) {
            //req.logStream.write('Failed to Send Email: ' + '\r\n' + error + '\r\n');
            res.send({ Success: false, Response: 'Failed to Send Email' });
        }
        else {
            res.send({ Success: true, Response: 'Email Sent Successfully' });
        }
    });
});

router.post('/support', function (req, res) {

    //req.logStream.write(JSON.stringify(req.body)+'\r\n');
    var To             = req.body.To;
    var ThreadTitle    = req.body.ThreadTitle;
    var Message        = req.body.Message;
    var Poster         = req.body.Poster;
    var TimeStamp      = req.body.TimeStamp;
    var TrackingNumber = req.body.TrackingNumber;

    //COMPOSE EMAIL
    var supportEmail = {
        from: "Municity Forum<support@sca-corp.com>", // sender address
        to: To, // receiver
        subject: 'Municity Forum - ' + ThreadTitle, // Subject line
        html: '<p><img src="https://municitymedia.com/M5EmailTemplate/MunicityHeaderDark.jpg"/></p>' +
              '<p style="padding:25px; width:575px;">' + Poster + ' posted at ' + TimeStamp + '<br><br>' + Message + '<br><br>' +
              'For the full thread, log in to the <a href=http://Municity5.com/scaforum>Forum</a> and search for the Tracking Number: ' + TrackingNumber + '<br></p>' +
              '<p><img src="https://municitymedia.com/M5EmailTemplate/MunicityFooterDark.jpg"/></p>',
        forceEmbeddedImages: true
    };
    //SEND FROM MUNICITY 5 AMAZON SES
    Municity5Transport.sendMail(supportEmail, function (error, response) {
        if (error) {
            //req.logStream.write('Failed to Send Email: ' + '\r\n' + error + '\r\n');
            res.send({ Success: false, Response: 'Failed to Send Email' });
        }
        else {
            //req.logStream.write('Email Sent: ' + '\r\n' + JSON.stringify(response) + '\r\n');
            res.send({ Success: true, Response: 'Email Sent Successfully' });
        }
    });

});

router.post('/', (req, res) => {
    const { from, to, subject, message, attachments, isCitizen } = req.body;

    const html = pug.renderFile(
        'views/email/communication.pug',
        { message, isCitizen },
        null
    );

    const email = {
        from: `Do Not Reply <${ from ? from : 'noreply@sca-corp.com' }>`,
        to,
        subject,
        html,
        attachments
    };

    sendEmail(res, email);
});


router.post('/custom', function (req, res) {
    console.log('Email!');

    //req.logStream.write(JSON.stringify(req.body)+'\r\n');
    var sendFrom       = req.body.sendFrom;
    var recipients     = req.body.recipients;
    var subject        = req.body.subject;
    var message        = req.body.message;
    var attachmentName = req.body.attachmentName;
    var attachmentPath = req.body.attachmentPath;

    //COMPOSE EMAIL
    var customEmail = {
        from: sendFrom, // sender address
        to: recipients, // list of receivers
        subject: subject, // Subject line
        html: message,
        forceEmbeddedImages: true
    };
    console.log('document and path!', attachmentName, attachmentPath)

    if (attachmentName && attachmentPath) {
        customEmail.attachments = [
            {
                filename: attachmentName,
                path: attachmentPath
            }
        ]
    }
    //SEND FROM MUNICITY 5 AMAZON SES
    Municity5Transport.sendMail(customEmail, function (error, response) {
        if (error) {
            //req.logStream.write('Failed to Send Email: ' + '\r\n' + error + '\r\n');
            res.send({ Success: false, Response: 'Failed to Send Email' });
        }
        else {
            //req.logStream.write('Email Sent: ' + '\r\n' + JSON.stringify(response) + '\r\n');
            res.send({ Success: true, Response: 'Email Sent Successfully' });
        }
    });
});

router.post('/welcome/client', (req, res) => {
    const { name, username, password } = req.body;

    const html = pug.renderFile(
        'views/email/welcome-client.pug',
        { name, username, password }, null
    );

    const email = {
        from: 'Support<support@sca-corp.com>',
        to: `${ name } <${ username }>`,
        subject: 'Welcome to M5',
        html
    };

    sendEmail(res, email);
});

router.post('/communication', (req, res) => {
    const { to, subject, message } = req.body;

    const html = pug.renderFile(
        'views/email/communication.pug',
        { message },
        null
    );

    const email = {
        from: 'Do Not Reply <noreply@sca-corp.com>',
        to,
        subject,
        html
    };

    sendEmail(res, email);
});

router.post('/:tenant', (req, res) => {
    const { from, to, subject, message, attachments, cc } = req.body;

    const email = {
        from: `Do Not Reply <${ from ? from : 'noreply@sca-corp.com' }>`,
        to,
        cc,
        subject,
        html: message,
        attachments
    };

    sendEmailHeaderFooter(res, req.params.tenant, email);
});

function sendEmailHeaderFooter(res, tenantName, email, opts) {
    email.html = email.html || '';

    const header = tenant.getHeader(tenantName);
    if (header) email.html = header + email.html;

    const footer = tenant.getFooter(tenantName);
    if (footer) email.html += footer;

    sendEmail(res, email, opts);
}

/** Sends an email given the proper transport. */
function sendEmail(res, email, opts) {
    const INTERNAL_SERVER_ERROR = 500;

    opts = opts || {};

    const transport = opts.transport || Municity5Transport;

    transport.sendMail(email, (error, response) => {
        if (error)
            res.status(INTERNAL_SERVER_ERROR).send({ Success: false, Response: 'Failed to send email!', Error: error });
        else
            res.send({ Success: true, Response: 'Email sent successfully!' });
    });
}

module.exports = router;
