//=================================================================================
// Municity Node
// File Management
//
// Prefix: 				/file
// Currently Supports: 	/create, /delete, /rename, /content, /content-recursive
//
// Author: 			Caleb Wright <cwright@sca-corp.com>
// Created Date: 	5/9/2014
// Last Modified: 	5/16/2014
//=================================================================================

const express = require('express');
const fs = require('fs');
const nodefs = require('node-fs');
const walk = require('walk');
const router = express.Router();
const request = require('request');
const merge = require('easy-pdf-merge');
const {exec} = require("child_process");

const fileMove = require('../controller/file/move')
const fileCopy = require('../controller/file/copy')
const fileConvert = require('../controller/file/convert')

const services = process.env.M5ServiceLocation;
var mediaDirectory = process.env.NodeDirectoryDrive + '/Municity Media/';

router.post('/convert', fileConvert);

router.post('/imagepdf', function (req, res) {
    var payload = req.body;
    var customer = payload.customer;
    var files = payload.files;
    var entity = payload.entity;
    var entity_ID = payload.entity_ID;
    var mergedName = payload.mergedName;
    var destinantion = `${mediaDirectory}/${customer}/${entity}/${entity_ID}/${mergedName}`;
    var destfilename = `/${entity}/${entity_ID}/${mergedName}`;

    var sourceFiles = '';
    for (var i = 0; i < files.length; i++) {
        //console.log('fileName', files[i].Name);
        var temp =
            sourceFiles += `"${mediaDirectory}/${customer}/${files[i].FilenameWeb}" `;
    }
    console.log('sourceFiles', sourceFiles);
//exec('magick  montage -tile 2x1 -resize 50% -label %f ' + sourceFiles + ' -pointsize 100 -geometry +0+0 "' + destinantion + '"', (error, stdout, stderr) => {
    exec('magick  montage -tile 1x2 -resize 20% -label %f ' + sourceFiles + ' -pointsize 100 -geometry +0+0 "' + destinantion + '"', (error, stdout, stderr) => {

        if (error) {
            console.log(`error: ${error.message}`);
            return;
        } else {
            writeToDocumentsTable(customer, entity, entity_ID, destfilename, function (success, resback) {
                console.log('wrote to documents');
                res.send({
                    Success: success,
                    Request: resback
                });
            })
        }
    });
});


router.post('/googleimage', function (req, res) {
    console.log('req', req);
    console.log('res', res);
    let payload = req.body,
        customer = payload.customer,
        entity = payload.entity,
        entity_ID = payload.entity_ID,
        url = payload.url,
        mapType = payload.mapType,
        destination = `${mediaDirectory}/${customer}/${entity}/${entity_ID}/`,
        images = payload.images,
        googleImgName = 'googleimage.png',
        destfilename = `${mediaDirectory}/${customer}/${entity}/${entity_ID}/${googleImgName}`,
        documentstablefilename = `/${entity}/${entity_ID}/${googleImgName}`,
        returnvalue = `/${customer}/${entity}/${entity_ID}/${googleImgName}`; // "return value" doesn't seem to be used

    // console.log('Document Table Name: ', documentstablefilename);
    // if (images && images.length > 0) {
    //
    // }

    // exists fxn is now deprecated-- https://stackoverflow.com/questions/17699599/node-js-check-if-file-exists
    // fs.exists(destination, function (exists) {
    if (fs.existsSync(destination)) {
        //CREATE PATH

        nodefs.mkdir(destination, 0o777, function (err) {
            download(url, destfilename, function () {
                console.log('done');

                writeToDocumentsTable(customer, entity, entity_ID, documentstablefilename, function (success, resback) {
                    console.log('wrote to documents');
                    res.send({
                        Success: success,
                        Request: resback
                    });
                })
            });
        });
    } else {
        download(url, destfilename, function () {
            console.log('done');
            writeToDocumentsTable(customer, entity, entity_ID, documentstablefilename, function (success, resback) {
                console.log('wrote to documents');
                res.send({
                    Success: success,
                    Request: resback
                });
            })
        });
    }
    // });////


});

var download = function (uri, filename, callback) {
    request.head(uri, function (err, res, body) {
        console.log('content-type:', res.headers['content-type']);
        console.log('content-length:', res.headers['content-length']);

        request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
    });
};


router.post('/mergepdf', function (req, res) {
    var payload = req.body;
    var customer = payload.customer;
    var files = payload.files;
    var entity = payload.entity;
    var entity_ID = payload.entity_ID;
    var mergedName = payload.mergedName;
    var destinantion = `${mediaDirectory}/${customer}/${entity}/${entity_ID}/${mergedName}`;
    var destfilename = `/${entity}/${entity_ID}/${mergedName}`;
    /*var currentPath = req.body.currentPath;

    var entity = req.body.entity;
    var entity_ID = req.body.entity_ID;
    var filename = req.body.filename;*/

    //var fullpath = `${mediaDirectory}/${customer}/${currentPath}`;
    //var extension = checkExtension(currentPath);
    var sourceFiles = [];
    for (var i = 0; i < files.length; i++) {
        //console.log('fileName', files[i].Name);
        sourceFiles.push(`${mediaDirectory}/${customer}/${files[i].FilenameWeb}`);
    }

    console.log('sourceFiles', sourceFiles);
    console.log('destinantion', destinantion);
    merge(sourceFiles, destinantion, function (err) {
        if (err) {
            return console.log(err)
        }
        writeToDocumentsTable(customer, entity, entity_ID, destfilename, function (success, resback) {
            console.log('wrote to documents');
            res.send({
                Success: success,
                Request: resback
            });
        })
    });

});


//CREATE NEW DIRECTORY
router.post('/create', function (req, res) {

    //req.logStream.write(JSON.stringify(req.body)+'\r\n');
    var createPath = req.body.path;

    //CHECK IF PATH EXISTS ALREADY
    fs.exists(mediaDirectory + createPath, function (exists) {
        if (!exists) {
            //CREATE PATH
            nodefs.mkdir(mediaDirectory + createPath, 0777, true, function (err) {
                if (err) {
                    //req.logStream.write('Error Creating Directory: ' + mediaDirectory + createPath + '\r\n' + err + '\r\n');
                    res.send({
                        Success: false,
                        Response: 'Failed to Create Directory'
                    });
                } else {
                    nodefs.mkdir(mediaDirectory + 'Thumbnails/' + createPath, 0777, true, function (err) {
                        if (err) {
                            //req.logStream.write('Error Creating Directory: ' + mediaDirectory + 'Thumbnails/' + createPath + '\r\n' + err + '\r\n');
                            res.send({
                                Success: false,
                                Response: 'Failed to Create Thumbnail Directory'
                            });
                        } else {
                            res.send({
                                Success: true,
                                Response: 'Directory Created Successfully'
                            });
                        }
                    });
                }
            });
        } else {
            res.send({
                Success: true,
                Response: 'Directory Already Exists'
            });
        }
    });

    //});
    req.on('error', function (e) {
        //req.logStream.write(e+'\r\n');
        res.send({
            Response: 'An Error Occured'
        })
    });
});

//MOVE A FILE OR DIRECTORY TO THE DELETED BRANCH OF MUNICITYMEDIA
router.post('/delete', function (req, res) {

    //req.logStream.write(JSON.stringify(req.body)+'\r\n');
    var path = req.body.path;
    var itemName = req.body.itemName;

    //CREATE DELETE DIRECTORY BEFORE MOVING
    nodefs.mkdir(mediaDirectory + 'DeletedFiles/' + path, 0777, true, function (err) {
        if (err) {
            //req.logStream.write('Error Creating Delete Directory: ' + mediaDirectory + 'DeletedFiles/' + path + '\r\n' + err + '\r\n');
            res.send({
                Success: false,
                Response: 'Failed to Create Delete Directory'
            });
        } else {
            //RENAME (MOVE) FILE OR FOLDER TO DELETE DIRECTORY
            fs.rename(mediaDirectory + path + '/' + itemName, mediaDirectory + 'DeletedFiles/' + path + '/' + itemName, function (err) {
                if (err) {
                    //req.logStream.write('Error Deleting Directory: ' + mediaDirectory + path + '/' + itemName + '\r\n' + err + '\r\n');
                    res.send({
                        Success: false,
                        Response: 'Failed to Delete File or Folder'
                    });
                } else {
                    //SAME PROCESS FOR THE THUMBNAIL DIRECTORY
                    nodefs.mkdir(mediaDirectory + 'DeletedFiles/' + 'Thumbnails/' + path, 0777, true, function (err) {
                        if (err) {
                            //req.logStream.write('Error Creating Thumbnail Delete Directory: ' + mediaDirectory + 'DeletedFiles/' + 'Thumbnails/' + path + '\r\n' + err + '\r\n');
                            res.send({
                                Success: true,
                                Response: 'File or Folder Deleted Successfully'
                            });
                        } else {
                            fs.rename(mediaDirectory + 'Thumbnails/' + path + '/' + itemName, mediaDirectory + 'DeletedFiles/' + 'Thumbnails/' + path + '/' + itemName, function (err) {
                                if (err) {
                                    //req.logStream.write('Error Deleting Thumbnail Directory: ' + mediaDirectory + 'Thumbnails/' + path + '/' + itemName + '\r\n' + err + '\r\n');
                                    res.send({
                                        Success: true,
                                        Response: 'File or Folder Deleted Successfully'
                                    });
                                } else {
                                    res.send({
                                        Success: true,
                                        Response: 'File or Folder Deleted Successfully'
                                    });
                                }
                            });
                        }
                    });

                }
            });
        }
    });
});

//RENAME A FILE OR FOLDER AND ITS THUMBNAIL
router.post('/rename', function (req, res) {

    //req.logStream.write(JSON.stringify(req.body)+'\r\n');
    var path = req.body.path;
    var currentName = req.body.currentName;
    var newName = req.body.newName;

    fs.rename(mediaDirectory + path + '/' + currentName, mediaDirectory + path + '/' + newName, function (err) {
        if (err) {
            //req.logStream.write('Error Renaming Directory: ' + mediaDirectory + path + '/' + currentName + '\r\n' + err + '\r\n');
            res.send({
                Success: false,
                Response: 'Failed to Rename File or Folder'
            });
        } else {
            fs.rename(mediaDirectory + 'Thumbnails/' + path + '/' + currentName, mediaDirectory + 'Thumbnails/' + path + '/' + newName, function (err) {
                //if(err) //req.logStream.write('Error Renaming Thumbnail Directory: ' + mediaDirectory + 'Thumbnails/' + path + '/' + currentName + '\r\n' + err + '\r\n');
            });
            res.send({
                Success: true,
                Response: 'File or Folder Renamed Successfully'
            });
        }
    });
});


router.post('/move', (req, res) => fileMove(req, res));

router.post('/copy', (req, res) => fileCopy(req, res));

//RETURN DIRECTORY CONTENT IN ARRAY FORM (NON RECURSIVE)
router.post('/content', function (req, res) {

    //req.logStream.write(JSON.stringify(req.body)+'\r\n');
    var path = req.body.path;

    nodefs.mkdir(mediaDirectory + path, 0777, true, function (err) {
        if (err) {
            //req.logStream.write('Error Creating Directory: ' + mediaDirectory + path + '\r\n' + err + '\r\n');
            res.send({
                Success: false,
                Response: 'Failed to Create Directory'
            });
        } else {
            fs.readdir(mediaDirectory + path, function (err, files) {
                if (err) {
                    //req.logStream.write('Failed Read Directory: ' + mediaDirectory + path + '\r\n' + err + '\r\n');
                    res.send({
                        Success: false,
                        Response: 'Failed Read Directory Content'
                    });
                } else {
                    res.send({
                        Success: true,
                        Response: 'Successfully Read Directory',
                        Content: files
                    });
                }
            });
        }
    });
});

//RETURN DIRECTORY CONTENT IN ARRAY FORM (RECURSIVE
router.post('/content-recursive', function (req, res) {

    //req.logStream.write(JSON.stringify(req.body)+'\r\n');
    var path = req.body.path;
    var customRoot = req.body.root;
    var files = [];

    nodefs.mkdir(mediaDirectory + path, 0777, true, function (err) {
        if (err) {
            //req.logStream.write('Error Creating Directory: ' + mediaDirectory + path + '\r\n' + err + '\r\n');
            res.send({
                Success: false,
                Response: 'Failed to Create Directory'
            });
        } else {
            //WALK DIRECTORY
            var walker = walk.walk(mediaDirectory + path, {
                followLinks: false
            });
            //ON FILE, REPLACE THE CURRENT ROOT WITH THE ROOT PARAM AND PUSH TO FILE ARRAY
            walker.on('file', function (root, stat, next) {
                root = root.replace(mediaDirectory + path, customRoot);
                files.push(root + '/' + stat.name);
                next();
            });
            //ON DIRECTORY, REPLACE ROOT WITH ROOT PARAM AND PUSH TO FILE ARRAY
            walker.on('directory', function (root, stat, next) {
                root = root.replace(mediaDirectory + path, customRoot);
                files.push(root + '/' + stat.name);
                next();
            });
            //RETURN FILE ARRAY
            walker.on('end', function () {
                res.send({
                    Success: true,
                    Response: 'Recursive Crawl Complete',
                    Content: files
                });
            });
        }
    });
});

//CREATE THUMBNAILS RECURSIVELY
router.post('/content-recursive-thumb', function (req, res) {

    //req.logStream.write(JSON.stringify(req.body)+'\r\n');
    var path = req.body.path;
    var customRoot = req.body.root;
    var files = [];

    var createThumb = function (mediaDirectory, filePath, fileName) {
        nodefs.mkdir(mediaDirectory + 'Thumbnails/' + filePath, 0777, true, function (err) {
            if (err) {
                //req.logStream.write('Error Creating Directory: ' + mediaDirectory + 'Thumbnails/' + filePath + '\r\n' + err + '\r\n');
            } else {
                try {
                    im.convert([mediaDirectory + filePath + '/' + fileName, '-resize', '200x200', mediaDirectory + 'Thumbnails/' + filePath + '/' + fileName],
                        function (err, stdout) {
                            if (err) {
                                //req.logStream.write('Error Creating Thumbnail' + err + '\r\n');
                            }
                        });
                } catch (error) {
                    //req.logStream.write('Error Attempting Thumbnail Creation' + error + '\r\n');
                }
            }

        });
    }

    nodefs.mkdir(mediaDirectory + path, 0777, true, function (err) {
        if (err) {
            //req.logStream.write('Error Creating Directory: ' + mediaDirectory + path + '\r\n' + err + '\r\n');
            res.send({
                Success: false,
                Response: 'Failed to Create Directory'
            });
        } else {
            //WALK DIRECTORY
            var walker = walk.walk(mediaDirectory + path, {
                followLinks: false
            });
            //ON FILE, REPLACE THE CURRENT ROOT WITH THE ROOT PARAM AND PUSH TO FILE ARRAY
            walker.on('file', function (root, stat, next) {
                var filePath = root.replace(mediaDirectory, '');
                createThumb(mediaDirectory, filePath, stat.name);
                next();
            });
            //ON DIRECTORY, REPLACE ROOT WITH ROOT PARAM AND PUSH TO FILE ARRAY
            walker.on('directory', function (root, stat, next) {

                next();
            });
            //RETURN FILE ARRAY
            walker.on('end', function () {
                res.send({
                    Success: true,
                    Response: 'Thumbnail Crawl Complete',
                    Content: files
                });
            });
        }
    });
});

function writeToDocumentsTable(customer, entity, entity_ID, fileName, callback) {
    console.log(customer, entity, entity_ID, fileName);

    var createDocumentRecordUrl = services + '/Customer/' + customer;
    createDocumentRecordUrl += '/' + entity;
    createDocumentRecordUrl += '/' + entity_ID;
    createDocumentRecordUrl += '/Document';
    // let createDocumentRecordUrl = 'https://mediaservice.municitymedia.com/file/upload?createDocuments=false&timestampImages=false';
    // let fileData = {
    //     "Section": entity,
    //     "SectionId": entity_ID,
    //     "Filename": fileName,
    //     "FilenameWeb": "Parcel/1681/FloorPlan.pdf",
    //     "Customer": customer,
    //     "Description": fileName + ' uploaded by ',
    //     "Type": 'Document',
    // }
    var fileData = {
        "Description": fileName + ' uploaded by ',
        "Name": fileName,
        "Type": 'Document',
        // "Directory": '' IDK why ? //fuck this attribute
    };
    fileData.Directory = '';
    //console.log(fileData);
    //console.log('createDocumentUrl: ', createDocumentRecordUrl);

    request({
        method: 'POST',
        url: createDocumentRecordUrl,
        json: fileData,
        gzip: true,
    }, function (error, response, body) {
        console.log('response', response, error);

        if (!error && response.statusCode == 200) {
            callback(true, response.body);
        } else {
            callback(false);
        }

    });
}

module.exports = router;
