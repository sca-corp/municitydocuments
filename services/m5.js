const _ = require('../util/util');
const request = require('request');

const BASE_M5SERVICES_URL = _.BASE_M5SERVICES_URL;

exports.postEntity = (customer, entity, body, callback, opts) => {
  const url = `${ BASE_M5SERVICES_URL }/Customer/${ customer }/${ entity }`;

  opts = Object.assign({
    method: 'POST',
    url: url,
    json: body,
    gzip: true,
  }, opts);

  request(opts, callback);
}

exports.postChildEntity = (customer, parent, parentId, entity, body, callback, opts) => {
  const url = `${ BASE_M5SERVICES_URL }/Customer/${ customer }/${ parent }/${ parentId }/${ entity }`;

  opts = Object.assign({
    method: 'POST',
    url: url,
    json: body,
    gzip: true,
  }, opts);

  request(opts, callback);
}

exports.patchEntity = (customer, entity, entityId, body, callback, opts) => {
  const url = `${ BASE_M5SERVICES_URL }/Customer/${ customer }/${ entity }/${ entityId }`;

  opts = Object.assign({
    method: 'PATCH',
    url: url,
    json: body,
    gzip: true,
  }, opts);

  request(opts, callback);
}

exports.deleteEntity = (entity, entityId, callback, opts) => {
  const url = `${ BASE_M5SERVICES_URL }/Customer/${ customer }/${ entity }/${ entityId }`;

  opts = Object.assign({
    method: 'DELETE',
    url: url,
    gzip: true,
  }, opts);

  request(opts, callback);
}
