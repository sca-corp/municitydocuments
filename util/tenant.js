const fs = require('fs');
const path = require('path');

const mediaDir = require('./util').mediaDir;

const headerPath = (tenant) => path.resolve(mediaDir, tenant, 'Header', 'default.html');
const footerPath = (tenant) => path.resolve(mediaDir, tenant, 'Footer', 'default.html');

exports.getHeader = (tenant) => {
  const path = headerPath(tenant);
  return fs.existsSync(path)
    ? fs.readFileSync(path)
    : '';
}

exports.getFooter = (tenant) => {
  const path = footerPath(tenant);
  return fs.existsSync(path)
    ? fs.readFileSync(path)
    : '';
}
