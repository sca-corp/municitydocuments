const fs = require('fs');
const file = require('../util/file')
const libre = require('libreoffice-convert');


const supportedExtensions = [
  'docx'
];

exports.convert = (inputFile, outputFile, format, callback) => {
  callback = callback || (() => {});

  isExtensionSupported(inputFile)
    ? fs.readFile(inputFile, onReadFile)
    : callback(new Error('Could not convert file because its extension is not supported. File: ' + inputFile + '.'))

  function onReadFile(err, inputFileData) {
    if (err) return callback(new Error('Could not convert file because the file does not exists. File: ' + inputFile + '.'));
    // Convert it to pdf format with undefined filter (see Libreoffice doc about filter)
    libre.convert(inputFileData, format, undefined, onLibreConvert)
  }

  function onLibreConvert(err, outputFileData) {
    if (err) return callback(new Error('Could not convert file. File: ' + inputFile));

    fs.writeFile(outputFile, outputFileData, (err) => onWriteFile(err, outputFileData))
  }

  function onWriteFile(err, outputFileData) {
    if (err) callback(new Error('Could not convert file because we could not write the file. File: ' + outputFile + '.'));
    else callback(null, outputFileData);
  }
}

function isExtensionSupported(filename) {
  const ext = file.extension(filename)
  return supportedExtensions.includes(ext);
}
