const fs = require('fs');
const sh = require('shelljs');

exports.BASE_M5SERVICES_URL = process.env.M5ServiceLocation
exports.mediaDir = process.env.NodeDirectoryDrive + '/Municity Media/';

exports.move = function (source, destination) {
  if (!fs.existsSync(source)) return { error: new Error(source + ' does not exists') };

  if (!fs.existsSync(destination)) {
    sh.mkdir('-p', destination);

    if (sh.error())
      return { error: new Error('Could not create ' + destination) };
  }


  sh.mv(source, destination);
  if (sh.error()) return { error: new Error('Could not move ' + source + ' to  ' + destination) };

  return { success: true };
}

exports.copy = function (source, destination) {
  if (!fs.existsSync(source)) return { error: new Error(source + ' does not exists') };

  if (!fs.existsSync(destination)) {
    sh.mkdir('-p', destination);

    if (sh.error())
      return { error: new Error('Could not create ' + destination) };
  }

  sh.cp('-Rf', source, destination);
  if (sh.error()) return { error: new Error('Could not copy ' + source + ' to  ' + destination) };

  return { success: true };
}
