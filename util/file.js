const path = require('path');

exports.extension = (filename) => path.extname(filename).split('.')[1];
