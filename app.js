//======================================================================
// Municity Documents
// Application Server
//
// Running on Port:   9040
// Current Routes:    /file, /email, /paypal, /setup, /upload, /export
//
// Author:          Caleb Wright <cwright@sca-corp.com>
// Created Date:    5/9/2014
// Last Modified:   11/21/2018
//======================================================================
var express = require('express');
var cors = require('cors');
var path = require('path');
var favicon = require('static-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var fs = require('fs');


var index = require('./routes/index');
var upload = require('./routes/upload');
var file = require('./routes/file');
var email = require('./routes/email');
var paypal = require('./routes/paypal');
var setup = require('./routes/setup');
var exports = require('./routes/export');
var notify = require('./routes/notification');
var documentGenerator = require('./routes/documentGenerator');

var directoryDriveLetter = process.env.NodeDirectoryDrive;
var logFolderPath = 'logs/';

var app = express();
if (process.env.Production == 'true') {
    console.log('Running in Production');
    var https = require('https');
    var options = {
        pfx: fs.readFileSync('MunicityMediaNodeBundle 20250126.pfx'),
        passphrase: 'N0RNMhhz'
        // cert: fs.readFileSync('gd_bundle-g2-g1.crt')
    };

    https.createServer(options, app).listen(9030);
} else {
    console.log('Running in Non-Production');
    app.listen(9030);
}


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(favicon());

//Check to make sure the log folder exists
if(!fs.existsSync(logFolderPath)){
  console.log('Creating log folder path at ' + logFolderPath);
  fs.mkdirSync(logFolderPath);
}

//GENERATE A LOG FILE EVERYTIME THE SERVER IS STARTED, NAMED BY DATE AND TIME
var date = new Date();
var logFile = logFolderPath + (date.getMonth() + 1) + '-' + date.getDate() + '-' + date.getFullYear() + ' ' + date.getHours() + '-' + date.getMinutes() + '.txt';
var logFileStream = fs.createWriteStream(logFile);

app.use(logger({
    format: ':date - :remote-addr - :method :url :status - :referrer',
    immediate: true,
    stream: logFileStream
}));
//---------------------------------------------------------------------------
app.use(bodyParser.json({
    limit: '500mb'
}));
app.use(bodyParser.urlencoded());
app.use(cookieParser());

// RESTRICT DOC ACCESS TO OFFICE IP ADDRESS
app.use(function (req, res, next) {
    /*if(req.client.socket._peername.address !== "50.74.189.206"){
        if(req.client._httpMessage.req.url == "/Doc.html"){
            res.render('RestrictedAccess');
        }else{
            next();
        }
    }else{*/
    next();
    //}
});
app.use(express.static(path.join(__dirname, 'public')));

//SERVE DOCUMENTATION INDEX PAGE
app.use('/', index);

// CORS CONFIGURATION
var accessArray;
if (process.env.Production == 'true') {
    accessArray = [
        'http://citysquared.com',
        'http://www.citysquared.com',
        'https://citysquared.com',
        'https://www.citysquared.com',

        'http://municity5.com',
        'http://www.municity5.com',
        'https://municity5.com',
        'https://www.municity5.com',

        'http://connect.municity5.com',
        'https://connect.municity5.com',

        'http://municityreports.com',
        'https://municityreports.com',
        'http://www.municityreports.com',
        'https://www.municityreports.com'
    ];    
} else {
    accessArray = [
        'http://municityutility.com',
        'http://www.municityutility.com',
        'http://test.municity5.com',
        'http://localhost',
    ];
}

/*
app.use(cors({ origin: accessArray }));
app.use(function (req, res, next) {
    req.logStream = logFileStream;
    next();
});
*/

app.use(function (req, res, next) {
    //CYCLE THROUGH ACCESS ARRAY AND SET ACCESS CONTROL ALLOW ORIGIN
    var accessOrigin = 'AccessOriginInvalid';
    var accessSet = true; //SET TO FALSE
    for (x = 0; x < accessArray.length; x++) {
        if (req.headers.origin == accessArray[x]) {
            accessOrigin = req.headers.origin;
            accessSet = true;
        }
    }
    //FIX FOR MUNICITY ENTERPRISE UPLOADS (PLEASANTVILLE)
    if (req.headers.from == "MunicityEnterprise") {
        accessSet = true;
    }
    if (req.headers.from == "MunicityMobile") {
        accessSet = true;
    }

    if (!accessSet) {
        res.set('Access-Control-Allow-Origin', accessOrigin);
        res.send({
            Success: false,
            Response: 'Access Denied'
        });
    } else {
        //SET DEFAULT HEADERS
        //res.set('Access-Control-Allow-Origin', accessOrigin);
        res.set('Access-Control-Allow-Origin', req.headers.origin);
        res.set('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, token, Authorization, X-Product, SourceId');
        //SET DEFAULT RESPONSE TYPE
        res.type('application/json');
        //SET REQUEST LOG STREAM
        req.logStream = logFileStream;
        //CHECK OPTIONS CALL
        if (req.method.toLowerCase() === 'options') {
            res.send({
                Response: 'Options Call'
            });
        } else {
            next();
        }
    }
});

//SERVICES
app.use('/upload', upload);
app.use('/file', file);
app.use('/email', email);
app.use('/paypal', paypal);
app.use('/setup', setup);
app.use('/export', exports);
app.use('/notification', notify);
app.use('/documents', documentGenerator);

/// catch 404 and forwarding to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers
// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

module.exports = app;
