# MunicityDocuments

![MunicityNode.png](https://bitbucket.org/repo/LekM8M/images/3992837524-MunicityNode.png)



## Prerequisites

* [Node.js](https://nodejs.org/en/) - Version 12.22.0
* [nssm](https://nssm.cc) - the Non-Sucking Service Manager

## Development

```
npm install
npm run start
```



## Deployment - Management

This section assumes that the following actions are performed on the Media Server.

#### Install Service

* Open **Command Prompt** and run it as an administrator

![cmd run as administrator.png](https://bitbucket.org/repo/LekM8M/images/2002325432-cmd%20run%20as%20administrator.png)

* Run `nssm install municitynode` on the cmd and fill out the **Application Path**, **Startup Directory**, and **Arguments** as in the picture bellow. Then click **Intall Service**.

    * **Application Path** - Path to the **node.exe** executable file.

    * **Startup Directory** - Path to the **municitydocuments** project.

    * **Arguments** - Main file to start the server.

      **NOTE**: the main file is the **bin\www** file and not the **app.js** file.

![nssm install municitynode.png](https://bitbucket.org/repo/LekM8M/images/1457321534-nssm%20install%20municitynode.png)



#### Start Service

* Run `nssm start municitynode`.

![nssm start municitynode.png](https://bitbucket.org/repo/LekM8M/images/3870769288-nssm%20start%20municitynode.png)

* Check https://municitymedia.com:9030.

![municitymedia home](/Users/zhunio/Desktop/untitled folder/municitymedia home.png)

#### Stop Service

* Run `nssm stop municitynode`

  ![nssm stop municitynode.png](https://bitbucket.org/repo/LekM8M/images/4250323343-nssm%20stop%20municitynode.png)

#### Edit Service

* Run `nssm edit municitynode`

![nssm edit municitynode.png](https://bitbucket.org/repo/LekM8M/images/1309754492-nssm%20edit%20municitynode.png)

#### Delete Service

* Run `nssm stop municitynode`

* Run `nssm remove municitynode confirm`

![nssm remove municitynode.png](https://bitbucket.org/repo/LekM8M/images/441899799-nssm%20remove%20municitynode.png)
