//======================================================================
// Municity Node
// Application Server
//
// Running on Port:   9040
// Current Routes:    /file, /email, /paypal, /setup, /upload, /export
//
// Author:          Caleb Wright <cwright@sca-corp.com>
// Created Date:    5/9/2014
// Last Modified:   5/12/2014
//======================================================================

var express         = require('express');
var path            = require('path');
var favicon         = require('static-favicon');
var logger          = require('morgan');
var cookieParser    = require('cookie-parser');
var bodyParser      = require('body-parser');
var fs              = require('fs');
var https           = require('https');

var index       = require('./routes/index');
var upload      = require('./routes/upload');
var file        = require('./routes/file');
var email       = require('./routes/email');
var paypal      = require('./routes/paypal');
var setup       = require('./routes/setup');
var exports     = require('./routes/export');
var notify      = require('./routes/notification');

var app     = express();
var options = {
    pfx: fs.readFileSync('MunicityMediaNodeBundle 20170126.pfx'),
    passphrase: 'J0yCloud'
//	cert: fs.readFileSync('gd_bundle-g2-g1.crt')
};

https.createServer(options, app).listen(9030);

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(favicon());
//GENERATE A LOG FILE EVERYTIME THE SERVER IS STARTED, NAMED BY DATE AND TIME
var date = new Date();
var logFile = 'logs/'+(date.getMonth()+1)+'-'+date.getDate()+'-'+date.getFullYear()+' '+date.getHours()+'-'+date.getMinutes()+'.txt';
var logFileStream = fs.createWriteStream(logFile);
app.use(logger({format:':date - :remote-addr - :method :url :status - :referrer', immediate: true, stream:logFileStream}));
//---------------------------------------------------------------------------
app.use(bodyParser.json({limit:'50mb'}));
app.use(bodyParser.urlencoded());
app.use(cookieParser());

//RESTRICT DOC ACCESS TO OFFICE IP ADDRESS
app.use(function(req, res, next) {
    /*if(req.client.socket._peername.address !== "24.39.106.238"){
        if(req.client._httpMessage.req.url == "/Doc.html"){
            res.render('RestrictedAccess');
        }else{
            next();
        }
    }else{*/
        next();
    //}
});
app.use(express.static(path.join(__dirname, 'public')));

//SERVE DOCUMENTATION INDEX PAGE
app.use('/', index);

var accessArray = [
    //MUNICITY TEST SERVER - UNCOMMENT FOR TEST NODE SERVER
    /*'http://municitytest.com',
    'http://www.municitytest.com',
    'http://test.municity5.com',*/
    'http://localhost',

    //MUNICITY PRODUCTION SERVER - UNCOMMENT FOR PRODUCTION NODE SERVER    
    'http://municity5.com',
    'http://www.municity5.com',
    'https://municity5.com',
    'https://www.municity5.com',

    'http://municityreports.com',
    'https://municityreports.com',
    'http://www.municityreports.com',
    'https://www.municityreports.com',

    'http://municitysupport.com',
    'https://municitysupport.com',
    'http://www.municitysupport.com',
    'https://www.municitysupport.com',

    'http://www.municitymobile.com',
    'https://www.municitymobile.com',
    'http://municitymobile.com',
    'https://municitymobile.com'
    
    //OPEN LOTPARKING - USED BY WEBSITE EMAIL FORM
    /*'http://openlotparking.com',
    'http://www.openlotparking.com'*/
    ];

app.use(function(req, res, next) {
    //CYCLE THROUGH ACCESS ARRAY AND SET ACCESS CONTROL ALLOW ORIGIN 
    var accessOrigin = 'AccessOriginInvalid';
    var accessSet = true; //SET TO FALSE
    for(x = 0; x < accessArray.length; x++){
        if(req.headers.origin == accessArray[x]){
            accessOrigin = req.headers.origin;
            accessSet = true;
        }
    }
    //FIX FOR MUNICITY ENTERPRISE UPLOADS (PLEASANTVILLE)
    if(req.headers.from == "MunicityEnterprise"){ accessSet = true; }
    if(req.headers.from == "MunicityMobile"){ accessSet = true; }

    if(!accessSet){
        res.set('Access-Control-Allow-Origin', accessOrigin);
        res.send({Success:false, Response:'Access Denied'});
    }else{    
        //SET DEFAULT HEADERS
        //res.set('Access-Control-Allow-Origin', accessOrigin);
        res.set('Access-Control-Allow-Origin', req.headers.origin);
        res.set('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, token, Authorization, X-Product');
        //SET DEFAULT RESPONSE TYPE
        res.type('application/json');
        //SET REQUEST LOG STREAM
        req.logStream = logFileStream;
        //CHECK OPTIONS CALL
        if(req.method.toLowerCase() === 'options'){
            res.send({Response:'Options Call'});
        }else{
            next();     
        }
    } 
});

//SERVICES
app.use('/upload', upload);
app.use('/file', file);
app.use('/email', email);
app.use('/paypal', paypal);
app.use('/setup', setup);
app.use('/export', exports);
app.use('/notification', notify);

/// catch 404 and forwarding to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});
/// error handlers
// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}
// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

module.exports = app;