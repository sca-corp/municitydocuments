const fs = require('fs');
const path = require('path');
const _ = require('../../util/util');


module.exports = function copy(req, res) {
  const src = req.body.currentPath;
  const dest = req.body.newPath;
  const file = req.body.itemName;

  const srcFile = path.resolve(_.mediaDir, src, file);
  const destDir = path.resolve(_.mediaDir, dest);

  const copiedFile = _.copy(srcFile, destDir);
  if (copiedFile.error) return res.status(500).send({
    Success: false,
    Response: 'Could not copy from ' + src + ' to ' + dest,
  })

  const srcThumbnail = path.resolve(_.mediaDir, 'Thumbnails', src, file);
  const destDirThumbnail = path.resolve(_.mediaDir, 'Thumbnails', dest);

  const copiedThumbnail = fs.existsSync(srcThumbnail) ?
                          _.copy(srcThumbnail, destDirThumbnail) : { error: true }

  if (copiedThumbnail.error)
    res.send({
      Success: true,
      Response: file + ' copied successfully but thumbnail was not'
    })
  else
    res.send({
      Success: true,
      Response: file + ' copied successfully'
    });
}
