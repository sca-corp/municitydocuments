const fs = require('fs');
const path = require('path');
const _ = require('../../util/util');

module.exports = function move(req, res) {
  const src = req.body.currentPath;
  const dest = req.body.newPath;
  const file = req.body.itemName;

  const srcFile = path.resolve(_.mediaDir, src, file);
  const destDir = path.resolve(_.mediaDir, dest);

  const movedFile = _.move(srcFile, destDir);
  if (movedFile.error) return res.status(500).send({
    Success: false,
    Response: 'Could not move from ' + src + ' to ' + dest,
  })

  const srcThumbnail = path.resolve(_.mediaDir, 'Thumbnails', src, file);
  const destDirThumbnail = path.resolve(_.mediaDir, 'Thumbnails', dest);

  const movedThumbnail = fs.existsSync(srcThumbnail) ?
                         _.move(srcThumbnail, destDirThumbnail) : { error: true }

  if (movedThumbnail.error)
    res.send({
      Success: true,
      Response: file + ' moved successfully but thumbnail was not'
    })
  else
    res.send({
      Success: true,
      Response: file + ' moved successfully'
    });
}
