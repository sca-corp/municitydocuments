const path = require('path');
const _ = require('../../util/util');
const m5 = require('../../services/m5');
const file = require('../../util/file');
const libre = require('../../util/libreoffice');

module.exports = function convert(req, res) {
  const customer = req.body.customer;
  const currentPath = req.body.currentPath;
  const entity = req.body.entity;
  const entityId = req.body.entity_ID;
  const filename = req.body.filename;

  const format = 'pdf';
  const extension = file.extension(filename);

  const inputFile = path.resolve(_.mediaDir, customer, currentPath);
  const outputFile = inputFile.replace(extension, format)

  libre.convert(inputFile, outputFile, format, (err) => {
    if (err) return res.status(501).send({
      Success: false,
      Response: err.message || 'Could not convert the file ' + inputFile + '.'
    });

    createDocument(customer, entity, entityId, 'Document', outputFile, (err, docRes, body) => {
      if (err) return res.status(501).send({
        Success: false,
        Response: err.message || 'Could not create the Document in the back end'
      });

      res.send({
        Success: true,
        Response: 'File converted successfully.',
        Document: body,
      })
    });
  })
}

function createDocument(customer, parent, parentId, entity, filename, callback) {
  const body = {
    Description: filename + ' Uploaded By Convert Service in Media Server',
    Name: filename,
    Type: 'Document',
    Directory: '',
  };

  m5.postChildEntity(customer, parent, parentId, entity, body, callback);
}
